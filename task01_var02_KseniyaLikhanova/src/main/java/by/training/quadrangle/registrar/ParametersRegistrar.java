package by.training.quadrangle.registrar;

import by.training.quadrangle.action.GeometricalParameter;
import by.training.quadrangle.entity.Point;
import by.training.quadrangle.entity.Quadrangle;
import by.training.quadrangle.observer.Observer;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

/**
 * The type Parameters registrar.
 * Created on 29.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class ParametersRegistrar implements Observer<List<Point>> {
    /**
     * Field parameter specifies object of GeometricalParameter.
     */
    private static GeometricalParameter parameter = new GeometricalParameter();
    /**
     * Field id specifies the perimeter of quadrangle.
     */
    private double perimeter;
    /**
     * Field id specifies the area of quadrangle.
     */
    private double area;

    /**
     * Instantiates a new Parameters registrar.
     * Subscribes for updates of quadrangle.
     *
     * @param quadrangle the quadrangle
     */
    public ParametersRegistrar(final Quadrangle quadrangle) {
        this.perimeter = parameter.calculatePerimeter(quadrangle.getVertices());
        this.area = parameter.calculateArea(quadrangle.getVertices());
        quadrangle.registerObserver(this);
    }

    /**
     * Gets perimeter.
     *
     * @return the perimeter
     */
    public double getPerimeter() {
        return perimeter;
    }

    /**
     * Gets area.
     *
     * @return the area
     */
    public double getArea() {
        return area;
    }

    /**
     * Update perimeter and area.
     *
     * @param vertices the vertices
     * @param msg      the massage for further action
     */
    @Override
    public void update(final List<Point> vertices, final String msg) {
        this.perimeter = parameter.calculatePerimeter(vertices);
        this.area = parameter.calculateArea(vertices);
    }

    @Override
    public String toString() {
        DecimalFormatSymbols s = new DecimalFormatSymbols();
        s.setDecimalSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("#0.0000", s);
        return "perimeter = " + decimalFormat.format(perimeter)
                + ", area = " + decimalFormat.format(area)
                + '}';
    }
}
