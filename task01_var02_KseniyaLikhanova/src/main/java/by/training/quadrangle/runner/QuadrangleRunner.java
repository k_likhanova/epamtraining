package by.training.quadrangle.runner;

import by.training.quadrangle.creator.QuadrangleCreator;
import by.training.quadrangle.customexception.IllegalObjectException;
import by.training.quadrangle.manager.QuadrangleManager;
import by.training.quadrangle.repository.QuadrangleRepository;
import by.training.quadrangle.specification.Specification;
import by.training.quadrangle.specification.search.AreaInRange;
import by.training.quadrangle.warehouse.ParametersRegistrarWareHouse;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

import static java.util.Arrays.asList;

/**
 * Class is point of entry in program.
 */
public class QuadrangleRunner {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("infoLogger");
    /**
     * Field FILE_PATH specifies the file name.
     */
    private static final String FILE_PATH = "data/main/QuadrangleData.txt";

    /**
     * Point of entry in program.
     *
     * @param args the arguments
     * @throws IllegalObjectException the custom illegal object exception
     * @throws IOException            the io exception
     */
    public static void main(final String[] args)
                        throws IllegalObjectException, IOException {
        QuadrangleManager manager = new QuadrangleManager();
        manager.fillQuadrangleRepository(FILE_PATH);
        LOG.log(Level.INFO, QuadrangleRepository.getInstance()
                                    .getQuadrangles().toString());
        LOG.log(Level.INFO, ParametersRegistrarWareHouse.getInstance()
                                    .getTriangleParameters().toString());
        QuadrangleCreator creator = new QuadrangleCreator();

        QuadrangleRepository.getInstance().add(creator.create(
                asList("Head", "0", "0", "1", "0", "1", "-1", "0", "-1")));
        LOG.log(Level.INFO, QuadrangleRepository.getInstance()
                                    .getQuadrangles().toString());
        LOG.log(Level.INFO, ParametersRegistrarWareHouse.getInstance()
                                    .getTriangleParameters().toString());

        Specification query = new AreaInRange(2, 2);
        LOG.log(Level.INFO, QuadrangleRepository.getInstance()
                                            .query(query).toString());
    }
}
