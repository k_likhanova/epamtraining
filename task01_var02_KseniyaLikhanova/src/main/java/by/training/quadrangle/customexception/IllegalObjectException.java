package by.training.quadrangle.customexception;

/**
 * The type Illegal object exception.
 * Created on 25.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class IllegalObjectException extends Exception {
    /**
     * Instantiates a new Illegal object exception.
     */
    public IllegalObjectException() {
        super();
    }

    /**
     * Instantiates a new Illegal object exception.
     *
     * @param message the message
     */
    public IllegalObjectException(final String message) {
        super(message);
    }
}
