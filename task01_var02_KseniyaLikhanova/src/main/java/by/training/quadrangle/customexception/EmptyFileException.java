package by.training.quadrangle.customexception;

import java.io.IOException;

/**
 * The type Empty file exception.
 * Created on 23.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class EmptyFileException extends IOException {
    /**
     * Instantiates a new Empty file exception.
     */
    public EmptyFileException() {
        super();
    }

    /**
     * Instantiates a new Empty file exception.
     *
     * @param message the message
     */
    public EmptyFileException(final String message) {
        super(message);
    }
}
