package by.training.quadrangle.factory;

import by.training.quadrangle.customexception.IllegalObjectException;
import by.training.quadrangle.entity.AbstractEntity;
import by.training.quadrangle.entity.Point;
import by.training.quadrangle.parser.Parser;
import by.training.quadrangle.validator.PointValidator;

import java.util.List;

/**
 * The type Point factory realizes pattern Factory Method.
 * Created on 25.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 2.0
 * @deprecated because quadrangle and point haven't general ancestor.
 */
@Deprecated
public class PointFactory extends EntityFactory {
    /**
     * The Parser.
     */
    private Parser parser;

    /**
     * Instantiates a new point factory.
     */
    public PointFactory() {
        parser = new Parser();
    }

    /**
     * Factory method.
     *
     * @return the point
     * @throws IllegalObjectException the illegal object exception
     */
    @Override
    public AbstractEntity factoryMethod(final List<String> fileStrings)
                                            throws IllegalObjectException {
        List<Double> coordinates = parser.parseStringToDouble(fileStrings);
        if (PointValidator.isPoint(coordinates)) {
            return new Point(coordinates.get(0), coordinates.get(1));
        } else  {
            throw new IllegalObjectException(
                    "Point must have 2 coordinates, but "
                    + coordinates.size()
                    + "coordinates has arrived.");
        }

    }
}
