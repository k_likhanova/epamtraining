package by.training.quadrangle.factory;

import by.training.quadrangle.customexception.IllegalObjectException;
import by.training.quadrangle.entity.AbstractEntity;
import by.training.quadrangle.entity.Point;
import by.training.quadrangle.entity.Quadrangle;
import by.training.quadrangle.parser.Parser;
import by.training.quadrangle.validator.QuadrangleValidator;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Quadrangle factory realizes pattern Factory Method.
 * Created on 25.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 * @deprecated because quadrangle and point haven't general ancestor.
 */
@Deprecated
public class QuadrangleFactory extends EntityFactory {
    /**
     * The Parser.
     */
    private Parser parser;

    /**
     * Instantiates a new quadrangle factory.
     */
    public QuadrangleFactory() {
        parser = new Parser();
    }

    /**
     * Factory method.
     *
     * @return the quadrangle
     * @throws IllegalObjectException the illegal object exception
     */
    @Override
    public AbstractEntity factoryMethod(final List<String> fileString)
                                                throws IllegalObjectException {
        List<Point> points = parser.parseStringToPoints(
                                fileString.stream()
                                          .skip(1)
                                          .collect(Collectors.toList()));
        if (QuadrangleValidator.isQuadrangle(points)) {
            return new Quadrangle(fileString.get(0), points);
        } else {
            throw new IllegalObjectException(
                    "Quadrangle with points " + points + "isn't exist.");
        }
    }
}
