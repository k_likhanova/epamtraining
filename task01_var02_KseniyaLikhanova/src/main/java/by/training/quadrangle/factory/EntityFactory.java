package by.training.quadrangle.factory;

import by.training.quadrangle.customexception.IllegalObjectException;
import by.training.quadrangle.entity.AbstractEntity;

import java.util.List;

/**
 * The abstract type Entity factory realizes pattern Factory Method.
 * Created on 25.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 2.0
 * @deprecated because hasn't general ancestor.
 */
@Deprecated
public abstract class EntityFactory {
    /**
     * Factory method.
     *
     * @param fileString the new file string
     * @return the abstract entity
     * @throws IllegalObjectException the illegal object exception
     */
    public abstract AbstractEntity factoryMethod(List<String> fileString)
                                                  throws IllegalObjectException;
}
