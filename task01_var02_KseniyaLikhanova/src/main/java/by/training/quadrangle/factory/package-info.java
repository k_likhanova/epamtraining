/**
 * Provides the classes necessary to create entities
 *   by means of Factory Method.
 * Classes for part A of task.
 */
@Deprecated
package by.training.quadrangle.factory;
