package by.training.quadrangle.entity;

/**
 * The abstract type AbstractEntity.
 * Created on 25.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 * @deprecated because point and Quadrangle aren't one abstract entity
 */
@Deprecated
public abstract class AbstractEntity {

}
