package by.training.quadrangle.entity;

import by.training.quadrangle.action.Generator;
import by.training.quadrangle.observer.Observable;
import by.training.quadrangle.observer.Observer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * The type Quadrangle.
 * Created on 20.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 2.0
 */
public class Quadrangle extends AbstractEntity
                        implements Observable<List<Point>> {
    /**
     * Field VERTICES_AMOUNT specifies the amount of vertices.
     */
    private static final int VERTICES_AMOUNT = 4;
    /**
     * Field id specifies the observers of quadrangle.
     */
    private List<Observer<List<Point>>> observers;
    /**
     * Field id specifies the id of quadrangle.
     */
    private final Long id;
    /**
     * Field name specifies the name of quadrangle.
     */
    private String name;
    /**
     * Field vertices specifies the vertices arrayList Quadrangle.
     */
    private List<Point> vertices;

    /**
     * Instantiates a new Quadrangle.
     */
    public Quadrangle() {
        observers = new LinkedList<>();
        id = Generator.generateId();
        name = "";
        vertices = new ArrayList<>(VERTICES_AMOUNT);
    }

    /**
     * Instantiates a new Quadrangle.
     *
     * @param newName       initialize the name of Quadrangle
     * @param newQuadrangle initialize the vertices arrayList of Quadrangle
     */
    public Quadrangle(final String newName, final List<Point> newQuadrangle) {
        observers = new LinkedList<>();
        id = Generator.generateId();
        this.name = newName;
        this.vertices = newQuadrangle;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Registers the name.
     *
     * @param newName initialize the name of Quadrangle
     */
    public void setName(final String newName) {
        this.name = newName;
    }

    /**
     * Gets vertices.
     *
     * @return the vertices
     */
    public List<Point> getVertices() {
        return vertices;
    }

    /**
     * Registers the vertices.
     *
     * @param newQuadrangle initialize the vertices arrayList of Quadrangle
     */
    public void setVertices(final List<Point> newQuadrangle) {
        this.vertices = newQuadrangle;
        notifyObservers(this.vertices, "");
    }

    /**
     * Gets vertices amount.
     *
     * @return the vertices amount
     */
    public static int getVerticesAmount() {
        return VERTICES_AMOUNT;
    }

    /**
     * Register observer.
     *
     * @param observer the observer
     */
    @Override
    public void registerObserver(final Observer<List<Point>> observer) {
        observers.add(observer);
    }

    /**
     * Remove observer.
     *
     * @param observer the observer
     */
    @Override
    public void removeObserver(final Observer observer) {
        observers.remove(observer);
    }

    /**
     * Notify observers.
     *
     * @param verticesOfQuadrangle the vertices
     * @param msg the massage for further action
     */
    @Override
    public void notifyObservers(final List<Point> verticesOfQuadrangle,
                                final String msg) {
        for (Observer<List<Point>> observer : observers) {
            observer.update(verticesOfQuadrangle, msg);
        }
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Quadrangle objQuadrangle = (Quadrangle) obj;
        Boolean isEqual = true;
        for (int i = 0; i < vertices.size(); i++) {
            if (isEqual) {
                isEqual = vertices.get(i).equals(
                                        objQuadrangle.getVertices().get(i));
            } else {
                break;
            }
        }
        return isEqual;
    }

    @Override
    public int hashCode() {
        final int primeNumber = 29;
        final int shiftNumber = 32;
        int hash = (int) (id ^ (id >>> shiftNumber));
        int nameHashCode = 0;
        for (int i = 0; i < name.length(); i++) {
            nameHashCode += name.charAt(i)
                            * (primeNumber ^ (name.length() - (i + 1)));
        }
        hash = hash * primeNumber + nameHashCode;
        for (Point vertex : vertices) {
            hash = hash * primeNumber + vertex.hashCode();
        }
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder(
                        "{ id = " + id + ", name = " + name);
        for (int i = 0; i < vertices.size(); i++) {
            if (i == 0) {
                buf.append("vertices{ ");
                buf.append(i + 1);
                buf.append(vertices.get(i));
            } else {
                buf.append(", ");
                buf.append(i + 1);
                buf.append(vertices.get(i));
            }
        }
        buf.append("};");
        return buf.toString();
    }
}
