package by.training.quadrangle.entity;

/**
 * The type Point.
 * Created on 20.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class Point extends AbstractEntity {
    /**
     * Field COORDINATES_AMOUNT specifies the amount of vertices.
     */
    private static final int COORDINATES_AMOUNT = 2;
    /**
     * Field x specifies the x-coordinate.
     */
    private double x;
    /**
     * Field y specifies the y-coordinate.
     */
    private double y;

    /**
     * Instantiates a new Point.
     */
    public Point() {
        super();
    }

    /**
     * Instantiates a new Point.
     *
     * @param newX initializes the x-coordinate.
     * @param newY initializes the y-coordinate.
     */
    public Point(final double newX, final double newY) {
        this.x = newX;
        this.y = newY;
    }

    /**
     * Instantiates a new Point.
     *
     * @param newPoint initializes the x-coordinate
     *                         and the y-coordinate.
     */
    public Point(final Point newPoint) {
        this.x = newPoint.getX();
        this.y = newPoint.getY();
    }

    /**
     * Registers the x-coordinate.
     *
     * @param newX initializes the x-coordinate.
     */
    public void setX(final double newX) {
        this.x = newX;
    }

    /**
     * Registers the y-coordinate.
     *
     * @param newY initializes the y-coordinate.
     */
    public void setY(final double newY) {
        this.y = newY;
    }

    /**
     * Gets the x-coordinate.
     *
     * @return the x-coordinate
     */
    public double getX() {
        return x;
    }

    /**
     * Gets the y-coordinate.
     *
     * @return the y-coordinate
     */
    public double getY() {
        return y;
    }

    /**
     * Gets coordinates amount.
     *
     * @return the coordinates amount
     */
    public static int getCoordinatesAmount() {
        return COORDINATES_AMOUNT;
    }

    /**
     * Registers the point.
     *
     * @param newX initializes the x-coordinate.
     * @param newY initializes the y-coordinate.
     */
    public void setPoint(final double newX, final double newY) {
        this.x = newX;
        this.y = newY;
    }

    /**
     * Registers the point.
     *
     * @param newPoint initializes the x-coordinate and the y-coordinate.
     */
    public void setPoint(final Point newPoint) {
        this.x = newPoint.getX();
        this.y = newPoint.getY();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Point point = (Point) obj;
        return Double.compare(this.x, point.getX()) == 0
                && Double.compare(this.y, point.getY()) == 0;
    }

    @Override
    public int hashCode() {
        final int primeNumber = 29;
        final int shiftNumber = 32;
        int hash;
        long temp;
        temp = Double.doubleToLongBits(this.x);
        hash = (int) (temp ^ (temp >>> shiftNumber));
        temp = Double.doubleToLongBits(this.y);
        hash = primeNumber * hash + (int) (temp ^ (temp >>> shiftNumber));
        return hash;
    }

    @Override
    public String toString() {
        return "( " + x + ", " + y + " )";
    }
}
