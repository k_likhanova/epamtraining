package by.training.quadrangle.creator;

import by.training.quadrangle.customexception.IllegalObjectException;
import by.training.quadrangle.entity.Point;
import by.training.quadrangle.parser.Parser;
import by.training.quadrangle.validator.PointValidator;

import java.util.List;

/**
 * The type Point creator.
 * Created on 04.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class PointCreator {
    /**
     * The Parser.
     */
    private Parser parser;

    /**
     * Instantiates a new point creator.
     */
    public PointCreator() {
        parser = new Parser();
    }

    /**
     * Create the Point.
     *
     * @param fileString the file strings
     * @return the point
     * @throws IllegalObjectException the illegal object exception
     */
    public Point create(final List<String> fileString)
                                            throws IllegalObjectException {
        List<Double> coordinates = parser.parseStringToDouble(fileString);
        if (PointValidator.isPoint(coordinates)) {
            return new Point(coordinates.get(0), coordinates.get(1));
        } else  {
            throw new IllegalObjectException(
                    "Point must have 2 coordinates, but "
                            + coordinates.size()
                            + "coordinates has arrived.");
        }

    }
}
