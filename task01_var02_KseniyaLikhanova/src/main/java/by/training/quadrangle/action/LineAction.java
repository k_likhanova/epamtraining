package by.training.quadrangle.action;

import by.training.quadrangle.entity.Point;

/**
 * The type Line action.
 * Created on 20.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class LineAction {
    /**
     * Calculate line length.
     *
     * @param point1 the start point of line.
     * @param point2 the end point of line.
     * @return the line length
     */
    public double calculateLineLength(final Point point1, final Point point2) {
        return Math.sqrt(Math.pow(point2.getX() - point1.getX(), 2)
                        + Math.pow(point2.getY() - point1.getY(), 2));
    }

    /**
     * Calculate distance between point and line.
     *
     * @param point          the point
     * @param startPointLine the start point line
     * @param endPointLine   the end point line
     * @return distance between point and line
     */
    public double calculateDistance(final Point point,
                                    final Point startPointLine,
                                    final Point endPointLine) {
        return Math.abs((endPointLine.getY() - startPointLine.getY())
                            * point.getX()
                        - (endPointLine.getX() - startPointLine.getX())
                            * point.getY()
                        + endPointLine.getX() * startPointLine.getY()
                        - endPointLine.getY() * startPointLine.getX())
                / calculateLineLength(startPointLine, endPointLine);
    }

    /**
     * Defines whether make three points a straight line.
     *
     * @param startPointLine the start point line
     * @param endPointLine   the end point line
     * @param point          the point
     * @return the boolean
     */
    public boolean isOnStraightLine(final Point startPointLine,
                                           final Point endPointLine,
                                           final Point point) {
        return calculateDistance(point, startPointLine, endPointLine) == 0;
    }

    /**
     * Defines whether the lines are parallel.
     *
     * @param startPointLine1 the start point line 1
     * @param endPointLine1   the end point line 1
     * @param startPointLine2 the start point line 2
     * @param endPointLine2   the end point line 2
     * @return the boolean
     */
    public boolean isParallel(
                      final Point startPointLine1, final Point endPointLine1,
                      final Point startPointLine2, final Point endPointLine2) {
        return !isIntersect(startPointLine1, endPointLine1,
                            startPointLine2, endPointLine2)
               && Double.compare(
                    calculateDistance(startPointLine1,
                                      startPointLine2, endPointLine2),
                    calculateDistance(endPointLine1,
                                      startPointLine2, endPointLine2))
                == 0;
    }

    /**
     * Defines whether the lines are crossed.
     * Search of position of each point concerning each line.
     *
     * @param startPointLine1 the start point line 1
     * @param endPointLine1   the end point line 1
     * @param startPointLine2 the start point line 2
     * @param endPointLine2   the end point line 2
     * @return the boolean
     */
    public boolean isIntersect(
                       final Point startPointLine1, final Point endPointLine1,
                       final Point startPointLine2, final Point endPointLine2) {
        int det1 = searchPointPositionConcerningLine(
                        startPointLine1, startPointLine2, endPointLine2);
        int det2 = searchPointPositionConcerningLine(
                        endPointLine1, startPointLine2, endPointLine2);
        int det3 = searchPointPositionConcerningLine(
                        startPointLine2, startPointLine1, endPointLine1);
        int det4 = searchPointPositionConcerningLine(
                        endPointLine2, startPointLine1, endPointLine1);
        return det1 * det2 <= 0 && det3 * det4 <= 0;
    }

    /**
     * Search of point position concerning a line.
     * return -1, if the point lies to the left of the line
     * return 0, if the point lies on the line
     * return 1, if the point lies to the right of the line*
     * det = determinant
     *
     * @param point      the point
     * @param startPoint the start point of line
     * @param endPoint   the end point of line
     * @return point position concerning a line
     */
    public int searchPointPositionConcerningLine(final Point point,
                                                 final Point startPoint,
                                                 final Point endPoint) {
        int signCompare;
        double det = (point.getX() - startPoint.getX())
                        * (endPoint.getY() - startPoint.getY())
                    - (point.getY() - startPoint.getY())
                        * (endPoint.getX() - startPoint.getX());
        if (det < 0) {
            signCompare = -1;
        } else if (det == 0) {
            signCompare = 0;
        } else {
            signCompare = 1;
        }
        return signCompare;
    }

    /**
     * Defines whether the line1 equals line2.
     *
     * @param startPointLine1 the start point line 1
     * @param endPointLine1   the end point line 1
     * @param startPointLine2 the start point line 2
     * @param endPointLine2   the end point line 2
     * @return the boolean
     */
    public boolean isEqualSides(
                       final Point startPointLine1, final Point endPointLine1,
                       final Point startPointLine2, final Point endPointLine2) {
        return Double.compare(
                        calculateLineLength(startPointLine1, endPointLine1),
                        calculateLineLength(startPointLine2, endPointLine2))
                == 0;
    }
}
