package by.training.quadrangle.action;

import by.training.quadrangle.entity.Point;

import java.util.List;

/**
 * The type GeometricalParameter.
 * Created on 20.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class GeometricalParameter {

    /**
     * Calculate perimeter of polygon.
     *
     * @param vertices the vertices of polygon
     * @return the perimeter
     */
    public double calculatePerimeter(final List<Point> vertices) {
        double perimeter = 0;
        final LineAction line = new LineAction();
        for (int i = 0; i < vertices.size(); i++) {
            if (i == vertices.size() - 1) {
                perimeter += line.calculateLineLength(
                                vertices.get(i),
                                vertices.get(0));
            } else {
                perimeter += line.calculateLineLength(
                                vertices.get(i),
                                vertices.get(i + 1));
            }
        }
        return perimeter;
    }

    /**
     * Calculate area of simple polygon.
     * By means of the trapezes areas.
     * Trapezes areas = i.x * ((i+1).y - (i-1).y) for each i.
     *
     * @param vertices the vertices of polygon
     * @return area double
     */
    public double calculateArea(final List<Point> vertices) {
        double area = 0;
        final int pointsArraySize = vertices.size();
        for (int i = 0; i < pointsArraySize; i++) {
            if (i == 0) {
                area += vertices.get(i).getX()
                        * (vertices.get(pointsArraySize - 1).getY()
                        - vertices.get(i + 1).getY());
            } else if (i == pointsArraySize - 1) {
                area += vertices.get(i).getX()
                        * (vertices.get(i - 1).getY()
                        - vertices.get(0).getY());
            } else {
                area += vertices.get(i).getX()
                        * (vertices.get(i - 1).getY()
                        - vertices.get(i + 1).getY());
            }
        }
        return area;
    }
}
