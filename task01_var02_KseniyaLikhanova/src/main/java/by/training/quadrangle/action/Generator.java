package by.training.quadrangle.action;

/**
 * The type Generator.
 * Created on 29.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public final class Generator {
    /**
     * Field idCounter specifies the id of quadrangle.
     */
    private static long idCounter;

    /**
     * Private constructor, because all method is static.
     */
    private Generator() {
    }

    /**
     * Generate id of quadrangle.
     *
     * @return the id
     */
    public static long generateId() {
        return ++idCounter;
    }
}
