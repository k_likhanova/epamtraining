package by.training.quadrangle.action;

import java.util.LinkedList;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * The type Transformation.
 * Created on 27.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public final class Transformation {
    /**
     * Private constructor, because all method is static.
     */
    private Transformation() { }

    /**
     * Transform List<Array> to List<List>.
     *
     * @param strings the strings array
     * @return the list
     */
    public static List<List<String>> transformArrayToList(
                                        final List<String[]> strings) {
        List<List<String>> listStrings = new LinkedList<>();
        for (String[] fileString : strings) {
            listStrings.add(asList(fileString));
        }
        return listStrings;
    }

    /**
     * Transform List<Double> to double[].
     *
     * @param numbers the strings array
     * @return the array
     */
    public static double[] transformListToArray(
            final List<Double> numbers) {
        return numbers.stream().mapToDouble(Number::doubleValue).toArray();
    }
}
