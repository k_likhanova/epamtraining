package by.training.quadrangle.action;

import by.training.quadrangle.entity.Point;
import by.training.quadrangle.entity.Quadrangle;
import by.training.quadrangle.validator.QuadrangleValidator;

import java.util.List;

/**
 * The boolean type Quadrangle kind.
 * Created on 21.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class QuadrangleKind {

    /**
     * Field FIRST_POINT the first point of a quadrangle.
     */
    private static final int FIRST_POINT = 0;
    /**
     * Field SECOND_POINT the second point of a quadrangle.
     */
    private static final int SECOND_POINT = 1;
    /**
     * Field THIRD_POINT the third point of a quadrangle.
     */
    private static final int THIRD_POINT = 2;
    /**
     * Field FOURTH_POINT the fourth point of a quadrangle.
     */
    private static final int FOURTH_POINT = 3;

    /**
     * Field line is used for actions with the lines.
     */
    private final LineAction line;

    /**
     * Default Constructor.
     * Instantiates a new lineAction.
     */
    public QuadrangleKind() {
        line = new LineAction();
    }

    /**
     * Defines whether the quadrangle is convex.
     * The polygon is simple.
     * All cross product of the adjacent sides will be the identical sign.
     *
     * @param quadrangle the quadrangle
     * @return the boolean
     */
    public boolean isConvex(final Quadrangle quadrangle) {
        List<Point> vertices = quadrangle.getVertices();
        int verticesAmount = vertices.size();
        boolean isConvex = true;
        boolean signCrossProduct = true;
        double vector1X;
        double vector1Y;
        double vector2X;
        double vector2Y;
        double crossProduct = 0;

        if (!QuadrangleValidator.isSimple(vertices)) {
            isConvex = false;
        }
        for (int i = 1; i <= verticesAmount; i++) {
            if (isConvex) {
                vector1X = vertices.get((i) % verticesAmount).getX()
                        - vertices.get((i - 1) % verticesAmount).getX();
                vector1Y = vertices.get((i) % verticesAmount).getY()
                        - vertices.get((i - 1) % verticesAmount).getY();
                vector2X = vertices.get((i + 1) % verticesAmount).getX()
                        - vertices.get((i) % verticesAmount).getX();
                vector2Y = vertices.get((i + 1) % verticesAmount).getY()
                        - vertices.get((i) % verticesAmount).getY();

                crossProduct = vector1X * vector2Y - vector1Y * vector2X;
            }
            if (i == 1) {
                signCrossProduct = crossProduct > 0;
            } else if (signCrossProduct != crossProduct > 0) {
                isConvex = false;
            }
        }
        return isConvex;
    }

    /**
     * Defines whether the quadrangle is parallelogram.
     *
     * @param quadrangle quadrangle
     * @return the boolean
     */
    public boolean isParallelogram(final Quadrangle quadrangle) {
        List<Point> vertices = quadrangle.getVertices();
        return line.isParallel(vertices.get(FIRST_POINT),
                               vertices.get(SECOND_POINT),
                               vertices.get(THIRD_POINT),
                               vertices.get(FOURTH_POINT))
                && line.isParallel(vertices.get(SECOND_POINT),
                                   vertices.get(THIRD_POINT),
                                   vertices.get(FOURTH_POINT),
                                   vertices.get(FIRST_POINT));
    }

    /**
     * Defines whether the polygon is equilateral.
     *
     * @param quadrangle quadrangle
     * @return the boolean
     */
    public boolean isEquilateral(final Quadrangle quadrangle) {
        List<Point> vertices = quadrangle.getVertices();
        boolean isEqual = true;
        for (int i = 1; i < vertices.size(); i++) {
            if (i == vertices.size() - 1) {
                isEqual = line.isEqualSides(
                            vertices.get(i - 1),
                            vertices.get(i),
                            vertices.get(i),
                            vertices.get(0));
            } else {
                isEqual = line.isEqualSides(
                            vertices.get(i - 1),
                            vertices.get(i),
                            vertices.get(i),
                            vertices.get(i + 1));
            }
            if (!isEqual) {
                break;
            }
        }
        return isEqual;
    }
    /**
     * Defines whether the quadrangle is rhombus.
     *
     * @param quadrangle the points arrayList of Quadrangle
     * @return the boolean
     */
    public boolean isRhombus(final Quadrangle quadrangle) {
        return isParallelogram(quadrangle)
                && isEquilateral(quadrangle);
    }

    /**
     * Defines whether the quadrangle is square.
     *
     * @param quadrangle quadrangle
     * @return the boolean
     */
    public boolean isSquare(final Quadrangle quadrangle) {
        List<Point> vertices = quadrangle.getVertices();
        return isRhombus(quadrangle)
                && Double.compare(
                        line.calculateLineLength(vertices.get(FIRST_POINT),
                                                 vertices.get(THIRD_POINT)),
                        line.calculateLineLength(vertices.get(SECOND_POINT),
                                                 vertices.get(FOURTH_POINT)))
                    == 0;
    }

    /**
     * Defines whether the quadrangle is trapezium.
     *
     * @param quadrangle quadrangle
     * @return the boolean
     */
    public boolean isTrapezium(final Quadrangle quadrangle) {
        List<Point> vertices = quadrangle.getVertices();
        return line.isParallel(vertices.get(FIRST_POINT),
                               vertices.get(SECOND_POINT),
                               vertices.get(THIRD_POINT),
                               vertices.get(FOURTH_POINT))
                || line.isParallel(vertices.get(SECOND_POINT),
                                   vertices.get(THIRD_POINT),
                                   vertices.get(FOURTH_POINT),
                                   vertices.get(FIRST_POINT));
    }
}
