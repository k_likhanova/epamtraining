/**
 * Provides the classes to performance for management of Quadrangle.
 * (For example: fill the repository of Quadrangle)
 */
package by.training.quadrangle.manager;
