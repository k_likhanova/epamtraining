package by.training.quadrangle.manager;

import by.training.quadrangle.creator.QuadrangleCreator;
import by.training.quadrangle.customexception.IllegalObjectException;
import by.training.quadrangle.entity.Quadrangle;
import by.training.quadrangle.parser.Parser;
import by.training.quadrangle.reader.FileDataReader;
import by.training.quadrangle.repository.QuadrangleRepository;
import by.training.quadrangle.warehouse.ParametersRegistrarWareHouse;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;

/**
 * The type Quadrangle manager.
 * Created on 04.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class QuadrangleManager {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("infoLogger");
    /**
     * Field reader specifies the object of FileDataReader.
     */
    private FileDataReader reader;
    /**
     * Field parser specifies the object of Parser.
     */
    private Parser parser;
    /**
     * Field creator specifies the object of Creator.
     */
    private QuadrangleCreator creator;

    /**
     * Instantiates a new Quadrangle manager.
     */
    public QuadrangleManager() {
        reader = new FileDataReader();
        parser = new Parser();
        creator = new QuadrangleCreator();
        QuadrangleRepository.getInstance().registerObserver(
                ParametersRegistrarWareHouse.getInstance());
    }

    /**
     * Fill quadrangle repository.
     *
     * @param fileName the file name
     * @throws IOException the io exception
     */
    public void fillQuadrangleRepository(final String fileName)
                                                        throws IOException {
        List<List<String>> fileStrings = parser.parseFileStrings(
                                                reader.readFromFile(fileName));
        for (List<String> fileString : fileStrings) {
            try {
                Quadrangle quadrangle = creator.create(fileString);
                QuadrangleRepository.getInstance().add(quadrangle);
                ParametersRegistrarWareHouse.getInstance().add(quadrangle);
            } catch (IllegalObjectException exc) {
                LOG.log(Level.INFO, exc.getMessage());
            }
        }
    }
}
