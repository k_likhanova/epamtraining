/**
 * Provides the class necessary to storage entities.
 * Pattern Singleton is used.
 */
package by.training.quadrangle.warehouse;
