package by.training.quadrangle.warehouse;

import by.training.quadrangle.entity.Quadrangle;
import by.training.quadrangle.observer.Observer;
import by.training.quadrangle.registrar.ParametersRegistrar;

import java.util.HashMap;
import java.util.Map;

/**
 * The type Parameters registrar ware house.
 * Created on 30.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public final class ParametersRegistrarWareHouse
                                implements Observer<Quadrangle> {
    /**
     * Field instance specifies the single object
     *              of ParametersRegistrarWareHouse.
     */
    private static ParametersRegistrarWareHouse instance
                            = new ParametersRegistrarWareHouse();
    /**
     * Field triangleParameters specifies the HashMap of Parameters registrar.
     * @see ParametersRegistrar
     */
    private Map<Long, ParametersRegistrar> triangleParameters;

    /**
     * Default constructor of ParametersRegistrarWareHouse.
     * Uses Singleton pattern therefor private.
     */
    private ParametersRegistrarWareHouse() {
        triangleParameters = new HashMap<>();
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static ParametersRegistrarWareHouse getInstance() {
        return instance;
    }

    /**
     * Gets triangle parameters.
     *
     * @return the triangle parameters
     */
    public Map<Long, ParametersRegistrar> getTriangleParameters() {
        return triangleParameters;
    }

    /**
     * Add new parameters registrar for quadrangle.
     *
     * @param quadrangle the quadrangle
     */
    public void add(final Quadrangle quadrangle) {
        triangleParameters.put(quadrangle.getId(),
                               new ParametersRegistrar(quadrangle));
    }

    /**
     * Remove parameters registrar of quadrangle.
     *
     * @param quadrangle the quadrangle
     */
    public void remove(final Quadrangle quadrangle) {
        triangleParameters.remove(quadrangle.getId());
    }


    /**
     * Update information.
     *
     * @param quadrangle the quadrangle
     * @param msg        the massage for further action
     */
    @Override
    public void update(final Quadrangle quadrangle, final String msg) {
        if (msg.equals("add")) {
            add(quadrangle);
        } else if (msg.equals("remove")) {
            remove(quadrangle);
        }
    }
}
