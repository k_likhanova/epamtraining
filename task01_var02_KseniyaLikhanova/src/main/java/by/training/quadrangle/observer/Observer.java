package by.training.quadrangle.observer;

/**
 * The interface Observer.
 * Created on 29.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 * @param <T> the type parameter of entity
 */
public interface Observer<T> {
    /**
     * Update information.
     *
     * @param entity the entities
     * @param msg the massage for further action
     */
    void update(T entity, String msg);
}
