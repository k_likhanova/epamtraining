package by.training.quadrangle.observer;

/**
 * The interface Observable.
 * Created on 29.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 * @param <T> the type parameter of entity
 */
public interface Observable<T> {
    /**
     * Register observer.
     *
     * @param observer the observer
     */
    void registerObserver(Observer<T> observer);

    /**
     * Remove observer.
     *
     * @param observer the observer
     */
    void removeObserver(Observer<T> observer);

    /**
     * Notify observers.
     *
     * @param entity the entities
     * @param msg the massage for further action
     */
    void notifyObservers(T entity, String msg);
}
