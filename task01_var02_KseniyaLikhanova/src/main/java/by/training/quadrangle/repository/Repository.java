package by.training.quadrangle.repository;

import by.training.quadrangle.entity.Point;
import by.training.quadrangle.specification.Specification;
import java.util.List;

/**
 * The interface Repository.
 * Created on 31.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 * @param <T> the type parameter of entity
 */
public interface Repository<T> {
    /**
     * Add entity.
     *
     * @param entity the entity
     */
    void add(T entity);

    /**
     * Remove entity.
     *
     * @param entity the entity
     */
    void remove(T entity);

    /**
     * Update entity.
     *
     * @param entity the entity
     * @param points the points
     */
    void update(T entity, List<Point> points);

    /**
     * Query.
     *
     * @param specification the specification of query
     * @return list of entity corresponding to specification
     */
    List<T> query(Specification specification);
}
