package by.training.quadrangle.repository;

import by.training.quadrangle.entity.Point;
import by.training.quadrangle.entity.Quadrangle;
import by.training.quadrangle.observer.Observable;
import by.training.quadrangle.observer.Observer;
import by.training.quadrangle.specification.Specification;
import by.training.quadrangle.specification.search.SearchSpecification;
import by.training.quadrangle.specification.sort.SortSpecification;

import java.util.LinkedList;
import java.util.List;

/**
 * The type Quadrangle repository.
 * Created on 31.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public final class QuadrangleRepository implements Repository<Quadrangle>,
                                                   Observable<Quadrangle> {
    /**
     * Field instance specifies the single object of QuadrangleRepository.
     */
    private static QuadrangleRepository instance
                            = new QuadrangleRepository();
    /**
     * Field id specifies the observers of quadrangleWareHouse.
     */
    private List<Observer<Quadrangle>> observers;
    /**
     * Field quadrangle specifies the quadrangles LinkedList of Quadrangle.
     */
    private List<Quadrangle> quadrangles;

    /**
     * Default constructor of QuadrangleWareHouse.
     * Uses Singleton pattern therefor private.
     */
    private QuadrangleRepository() {
        quadrangles = new LinkedList<>();
        observers = new LinkedList<>();
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static QuadrangleRepository getInstance() {
        return instance;
    }

    /**
     * Gets quadrangles.
     *
     * @return the quadrangles
     */
    public List<Quadrangle> getQuadrangles() {
        return quadrangles;
    }

    /**
     * Add quadrangle.
     *
     * @param newQuadrangle the new quadrangle
     */
    @Override
    public void add(final Quadrangle newQuadrangle) {
        quadrangles.add(newQuadrangle);
        notifyObservers(newQuadrangle, "add");
    }

    /**
     * Remove quadrangle.
     *
     * @param quadrangle the quadrangle
     */
    @Override
    public void remove(final Quadrangle quadrangle) {
        quadrangles.remove(quadrangle);
        notifyObservers(quadrangle, "remove");
    }

    /**
     * Update quadrangle.
     *
     * @param quadrangle the quadrangle
     * @param newVertices the newVertices
     */
    @Override
    public void update(final Quadrangle quadrangle,
                       final List<Point> newVertices) {
        quadrangle.setVertices(newVertices);
    }


    /**
     * Query.
     *
     * @param specification the specification of query
     * @return list of quadrangle corresponding to specification
     */
    @Override
    public List<Quadrangle> query(final Specification specification) {
        List<Quadrangle> resultList = new LinkedList<>();
        if (specification instanceof SearchSpecification
                || specification instanceof SortSpecification) {
            resultList = specification.specify(quadrangles);
        }
        return resultList;
    }

    /**
     * Register observer.
     *
     * @param observer the observer
     */
    @Override
    public void registerObserver(final Observer<Quadrangle> observer) {
        observers.add(observer);
    }

    /**
     * Remove observer.
     *
     * @param observer the observer
     */
    @Override
    public void removeObserver(final Observer observer) {
        observers.remove(observer);
    }

    /**
     * Notify observers.
     *
     * @param quadrangle the quadrangle
     * @param msg the massage for further action
     */
    @Override
    public void notifyObservers(final Quadrangle quadrangle, final String msg) {
        for (Observer<Quadrangle> observer : observers) {
            observer.update(quadrangle, msg);
        }
    }
}
