/**
 * Provides the interface for Repository pattern.
 * Provides the class necessary to storage of a collection
 *    which contains entities and can filter
 *    and return the result according to query.
 * Pattern Repository and Singleton is used.
 */
package by.training.quadrangle.repository;
