package by.training.quadrangle.validator;

import by.training.quadrangle.action.LineAction;
import by.training.quadrangle.entity.Point;
import by.training.quadrangle.entity.Quadrangle;

import java.util.List;

/**
 * The quadrangle validator.
 * Created on 24.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public final class QuadrangleValidator {
    /**
     * Field line specifies object of LineAction.
     */
    private static LineAction line = new LineAction();

    /**
     * Private constructor, because all method is static.
     */
    private QuadrangleValidator() { }

    /**
     * Defines whether the polygon is quadrangle.
     *
     * @param vertices the vertices of polygon
     * @return the boolean
     */
    public static boolean isQuadrangle(final List<Point> vertices) {
        return vertices.size() == Quadrangle.getVerticesAmount()
               && !isContainsThreePointLine(vertices)
               && isSimple(vertices);
    }

    /**
     * Defines whether the polygon contains three point on line.
     *
     * @param vertices the vertices of polygon
     * @return the boolean
     */
    public static boolean isContainsThreePointLine(final List<Point> vertices) {
        boolean isFlagOfContains = false;
        for (int i = 0; i < vertices.size() - 1; i++) {
            if (i == vertices.size() - 2) {
                isFlagOfContains = line.isOnStraightLine(vertices.get(i),
                                                    vertices.get(i + 1),
                                                    vertices.get(0));
            } else {
                isFlagOfContains = line.isOnStraightLine(vertices.get(i),
                                                    vertices.get(i + 1),
                                                    vertices.get(i + 2));
            }
            if (isFlagOfContains) {
                break;
            }
        }
        return isFlagOfContains;
    }



    /**
     * Defines whether the polygon is Simple.
     * Polygon has n sides.
     * Defines whether the line(i) and line(i+1) are crossed.
     * If i equal n than defines whether the line(i) and line(1) are crossed.
     * If there are no crossings, then the polygon is simple
     *
     * @param vertices the vertices of polygon
     * @return the boolean
     */
    public static boolean isSimple(final List<Point> vertices) {
        int verticesAmount = vertices.size();
        boolean isFlagOfSimple = true;
        for (int i = 0, j = 2; i < verticesAmount - 2; i++, j++) {
                if (j == verticesAmount - 1) {
                    isFlagOfSimple = !line.isIntersect(
                                        vertices.get(i), vertices.get(i + 1),
                                        vertices.get(j), vertices.get(0));
                } else {
                    isFlagOfSimple = !line.isIntersect(
                                        vertices.get(i), vertices.get(i + 1),
                                        vertices.get(j), vertices.get(j + 1));
                }
            if (!isFlagOfSimple) {
                break;
            }
        }
        return isFlagOfSimple;
    }
}
