package by.training.quadrangle.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The type File strings validator.
 * Created on 24.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public final class FileValidator {
    /**
     * Default constructor.
     */
    private FileValidator() {
    }

    /**
     * Validate line boolean.
     *
     * @param fileString the parameter
     * @param regex      the regular expression
     * @return the boolean
     */
    public static boolean validateLine(final String fileString,
                                       final String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(fileString);
        return matcher.matches();
    }
}
