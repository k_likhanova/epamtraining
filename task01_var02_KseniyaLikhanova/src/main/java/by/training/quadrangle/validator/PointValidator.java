package by.training.quadrangle.validator;

import by.training.quadrangle.entity.Point;

import java.util.List;

/**
 * The point validator.
 * Created on 04.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public final class PointValidator {

    /**
     * Private constructor, because all method is static.
     */
    private PointValidator() {
    }

    /**
     * Defines whether the double array is point.
     *
     * @param coordinates the coordinates of point
     * @return the boolean
     */
    public static boolean isPoint(final List<Double> coordinates) {
        return coordinates.size() == Point.getCoordinatesAmount();
    }
}
