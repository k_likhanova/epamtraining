package by.training.quadrangle.validator;

/**
 * The type Regular expression.
 * Created on 04.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public final class RegEx {
    /**
     * Regular expression for quadrangle name + double coordinates.
     */
    public static final String FILE_STRING_PATTERN =
                                   "\\s*[A-Z]\\w*(\\s*-?\\d+(\\.\\d+)?\\s*){8}";

    /**
     * Regular expression for sting from doubles.
     */
    public static final String DOUBLE_STRING_PATTERN =
                                   "(\\s*-?\\d+(\\.\\d+)?\\s*)+";

    /**
     * Default constructor. private because all field static.
     */
    private RegEx() {
    }
}
