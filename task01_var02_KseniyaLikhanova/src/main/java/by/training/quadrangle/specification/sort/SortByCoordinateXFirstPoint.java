package by.training.quadrangle.specification.sort;

import by.training.quadrangle.entity.Quadrangle;

import java.util.Comparator;
import java.util.List;

/**
 * The type Sort by coordinate x first point.
 * Created on 31.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class SortByCoordinateXFirstPoint implements SortSpecification {
    /**
     * Sort quadrangles by coordinate x first point.
     *
     * @param quadrangles the quadrangles
     * @return the quadrangles list is sorted by coordinate x first point
     */
    @Override
    public List<Quadrangle> specify(final List<Quadrangle> quadrangles) {
        quadrangles.sort(Comparator.comparingDouble(
                                    q -> q.getVertices().get(0).getX()));
        return quadrangles;
    }
}
