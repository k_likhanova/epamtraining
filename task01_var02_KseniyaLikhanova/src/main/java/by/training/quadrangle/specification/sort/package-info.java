/**
 * Provides sort specification classes necessary to query.
 */
package by.training.quadrangle.specification.sort;
