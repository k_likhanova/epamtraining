package by.training.quadrangle.specification.search;

import by.training.quadrangle.action.QuadrangleKind;
import by.training.quadrangle.entity.Quadrangle;

/**
 * The type Search rhombus.
 * Created on 04.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class SearchRhombus extends SearchSpecification {
    /**
     * Field instance kind of quadrangle.
     */
    private QuadrangleKind kind;

    /**
     * Instantiates a new quadrangle kind.
     */
    public SearchRhombus() {
        kind = new QuadrangleKind();
    }

    /**
     * Defines whether the quadrangle is rhombus.
     *
     * @param quadrangle the points arrayList of Quadrangle
     * @return the boolean
     */
    @Override
    boolean isSatisfiedBy(final Quadrangle quadrangle) {
        return kind.isRhombus(quadrangle);
    }
}
