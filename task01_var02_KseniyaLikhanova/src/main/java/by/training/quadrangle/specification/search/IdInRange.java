package by.training.quadrangle.specification.search;

import by.training.quadrangle.entity.Quadrangle;

/**
 * The type search id in range.
 * Created on 03.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class IdInRange extends SearchSpecification {
    /**
     * Field instance min id in range.
     */
    private final long minId;
    /**
     * Field instance max id in range.
     */
    private final long maxId;

    /**
     * Instantiates a new range of id.
     *
     * @param newMinId the new min id
     * @param newMaxId the new max id
     */
    public IdInRange(final long newMinId, final long newMaxId) {
        this.minId = newMinId;
        this.maxId = newMaxId;
    }

    /**
     * Whether id of quadrangle in range.
     *
     * @param quadrangle the quadrangle
     * @return the boolean
     */
    @Override
    public boolean isSatisfiedBy(final Quadrangle quadrangle) {
        return Long.compare(quadrangle.getId(), minId) > -1
                && Long.compare(quadrangle.getId(), maxId) < 1;
    }
}
