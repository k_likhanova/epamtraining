package by.training.quadrangle.specification.search;

import by.training.quadrangle.entity.Point;
import by.training.quadrangle.entity.Quadrangle;

/**
 * The type search quadrangle which has vertices in the third quadrant.
 * Created on 03.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class VerticesIn3Quadrant extends SearchSpecification {


    /**
     * Do vertices of quadrangle lie in the third quadrant.
     *
     * @param quadrangle the quadrangle
     * @return the boolean
     */
    @Override
    boolean isSatisfiedBy(final Quadrangle quadrangle) {
        boolean isTheFirstQuadrangle = true;
        for (Point vertex : quadrangle.getVertices()) {
            if (Double.compare(vertex.getX(), 0) > -1
                    || Double.compare(vertex.getY(), 0) > -1) {
                isTheFirstQuadrangle = false;
                break;
            }
        }
        return isTheFirstQuadrangle;
    }
}
