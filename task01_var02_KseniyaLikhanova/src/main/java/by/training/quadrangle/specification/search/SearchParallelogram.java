package by.training.quadrangle.specification.search;

import by.training.quadrangle.action.QuadrangleKind;
import by.training.quadrangle.entity.Quadrangle;

/**
 * The type Search parallelogram.
 * Created on 04.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class SearchParallelogram extends SearchSpecification {
    /**
     * Field instance kind of quadrangle.
     */
    private QuadrangleKind kind;

    /**
     * Instantiates a new quadrangle kind.
     */
    public SearchParallelogram() {
        kind = new QuadrangleKind();
    }

    /**
     * Defines whether the quadrangle is parallelogram.
     *
     * @param quadrangle the points arrayList of Quadrangle
     * @return the boolean
     */
    @Override
    boolean isSatisfiedBy(final Quadrangle quadrangle) {
        return kind.isParallelogram(quadrangle);
    }
}
