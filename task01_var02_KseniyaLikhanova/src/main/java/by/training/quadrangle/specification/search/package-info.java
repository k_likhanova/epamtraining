/**
 * Provides search specification classes necessary to query.
 */
package by.training.quadrangle.specification.search;
