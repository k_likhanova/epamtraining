package by.training.quadrangle.specification.search;

import by.training.quadrangle.entity.Quadrangle;
import by.training.quadrangle.warehouse.ParametersRegistrarWareHouse;

/**
 * The type search perimeter in range.
 * Created on 03.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class PerimeterInRange extends SearchSpecification {
    /**
     * Field instance min perimeter in range.
     */
    private final double minPerimeter;
    /**
     * Field instance max perimeter in range.
     */
    private final double maxPerimeter;

    /**
     * Instantiates new range of perimeter.
     *
     * @param newMinPerimeter the new min perimeter
     * @param newMaxPerimeter the new max perimeter
     */
    public PerimeterInRange(final double newMinPerimeter,
                            final double newMaxPerimeter) {
        this.minPerimeter = newMinPerimeter;
        this.maxPerimeter = newMaxPerimeter;
    }

    /**
     * Whether perimeter of quadrangle in range.
     *
     * @param quadrangle the quadrangle
     * @return the boolean
     */
    @Override
    public boolean isSatisfiedBy(final Quadrangle quadrangle) {
        double quadranglePerimeter = ParametersRegistrarWareHouse.getInstance()
                                        .getTriangleParameters()
                                        .get(quadrangle.getId()).getPerimeter();
        return Double.compare(quadranglePerimeter, minPerimeter) > -1
                && Double.compare(quadranglePerimeter, maxPerimeter) < 1;
    }
}
