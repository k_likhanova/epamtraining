package by.training.quadrangle.specification.sort;

import by.training.quadrangle.entity.Quadrangle;
import by.training.quadrangle.warehouse.ParametersRegistrarWareHouse;

import java.util.Comparator;
import java.util.List;

/**
 * The type Sort by perimeter.
 * Created on 03.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class SortByPerimeter implements SortSpecification {
    /**
     * Sort quadrangles by perimeter.
     *
     * @param quadrangles the quadrangles
     * @return the quadrangles list is sorted by perimeter
     */
    @Override
    public List<Quadrangle> specify(final List<Quadrangle> quadrangles) {
        quadrangles.sort(Comparator.comparingDouble(
                q -> ParametersRegistrarWareHouse.getInstance()
                        .getTriangleParameters()
                        .get(q.getId()).getPerimeter()));
        return quadrangles;
    }
}
