package by.training.quadrangle.specification.search;

import by.training.quadrangle.action.QuadrangleKind;
import by.training.quadrangle.entity.Quadrangle;

/**
 * The type Search trapezium.
 * Created on 04.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class SearchTrapezium extends SearchSpecification {
    /**
     * Field instance kind of quadrangle.
     */
    private QuadrangleKind kind;

    /**
     * Instantiates a new quadrangle kind.
     */
    public SearchTrapezium() {
        kind = new QuadrangleKind();
    }

    /**
     * Defines whether the quadrangle is trapezium.
     *
     * @param quadrangle the points arrayList of Quadrangle
     * @return the boolean
     */
    @Override
    boolean isSatisfiedBy(final Quadrangle quadrangle) {
        return kind.isTrapezium(quadrangle);
    }
}
