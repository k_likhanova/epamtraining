package by.training.quadrangle.specification.sort;

import by.training.quadrangle.specification.Specification;

/**
 * The interface Sort specification.
 * Created on 31.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public interface SortSpecification extends Specification {
}
