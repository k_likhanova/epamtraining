package by.training.quadrangle.specification.sort;

import by.training.quadrangle.entity.Quadrangle;
import by.training.quadrangle.warehouse.ParametersRegistrarWareHouse;

import java.util.Comparator;
import java.util.List;

/**
 * The type Sort by area.
 * Created on 03.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class SortByArea implements SortSpecification {
    /**
     * Sort quadrangles by area.
     *
     * @param quadrangles the quadrangles
     * @return the quadrangles list is sorted by area
     */
    @Override
    public List<Quadrangle> specify(final List<Quadrangle> quadrangles) {
        quadrangles.sort(Comparator.comparingDouble(
                            q -> ParametersRegistrarWareHouse.getInstance()
                                    .getTriangleParameters()
                                    .get(q.getId()).getArea()));
        return quadrangles;
    }
}
