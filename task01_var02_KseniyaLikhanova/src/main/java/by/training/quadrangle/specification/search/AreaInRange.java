package by.training.quadrangle.specification.search;

import by.training.quadrangle.entity.Quadrangle;
import by.training.quadrangle.warehouse.ParametersRegistrarWareHouse;

/**
 * The type search area in range.
 * Created on 03.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class AreaInRange extends SearchSpecification {
    /**
     * Field instance min area in range.
     */
    private final double minArea;
    /**
     * Field instance max area in range.
     */
    private final double maxArea;

    /**
     * Instantiates new range of area.
     *
     * @param newMinArea the new min area
     * @param newMaxArea the new max area
     */
    public AreaInRange(final double newMinArea, final double newMaxArea) {
        this.minArea = newMinArea;
        this.maxArea = newMaxArea;
    }

    /**
     * Whether area of quadrangle in range.
     *
     * @param quadrangle the quadrangle
     * @return the boolean
     */
    @Override
    public boolean isSatisfiedBy(final Quadrangle quadrangle) {
        double quadrangleArea = ParametersRegistrarWareHouse.getInstance()
                                .getTriangleParameters()
                                .get(quadrangle.getId()).getArea();
        return Double.compare(quadrangleArea, minArea) > -1
                && Double.compare(quadrangleArea, maxArea) < 1;
    }
}
