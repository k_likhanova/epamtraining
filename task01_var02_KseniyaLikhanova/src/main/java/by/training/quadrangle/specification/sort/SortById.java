package by.training.quadrangle.specification.sort;

import by.training.quadrangle.entity.Quadrangle;

import java.util.Comparator;
import java.util.List;

/**
 * The type Sort by id.
 * Created on 31.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class SortById implements SortSpecification {
    /**
     * Sort quadrangles by id.
     *
     * @param quadrangles the quadrangles
     * @return the quadrangles list is sorted by id
     */
    @Override
    public List<Quadrangle> specify(final List<Quadrangle> quadrangles) {
        quadrangles.sort(Comparator.comparing(Quadrangle::getId));
        return quadrangles;
    }
}
