package by.training.quadrangle.specification;

import by.training.quadrangle.entity.Quadrangle;

import java.util.List;

/**
 * The interface Specification.
 * Created on 31.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public interface Specification {
    /**
     * Specify list according to query.
     *
     * @param quadrangles the quadrangles
     * @return the list definite by query
     */
    List<Quadrangle> specify(List<Quadrangle> quadrangles);
}
