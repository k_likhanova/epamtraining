package by.training.quadrangle.specification.search;

import by.training.quadrangle.entity.Quadrangle;
import by.training.quadrangle.specification.Specification;

import java.util.LinkedList;
import java.util.List;

/**
 * The interface Search specification.
 * Created on 31.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public abstract class SearchSpecification implements Specification {
    /**
     * Is satisfied by query.
     *
     * @param quadrangle the quadrangle
     * @return the boolean
     */
    abstract boolean isSatisfiedBy(Quadrangle quadrangle);

    /**
     * Specify list according to query.
     *
     * @param quadrangles the quadrangles
     * @return the quadrangles definite by query
     */
    @Override
    public List<Quadrangle> specify(final List<Quadrangle> quadrangles) {
        List<Quadrangle> resultQuadrangles = new LinkedList<>();
        for (Quadrangle quadrangle : quadrangles) {
            if (isSatisfiedBy(quadrangle)) {
                resultQuadrangles.add(quadrangle);
            }
        }
        return resultQuadrangles;
    }
}
