package by.training.quadrangle.reader;

import by.training.quadrangle.customexception.EmptyFileException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The type File data reader.
 * Created on 23.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class FileDataReader {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("errorLogger");

    /**
     * Read from file list of strings.
     *
     * @param fileName the file name
     * @return the list of strings
     * @throws IOException the io exception
     */
    public List<String> readFromFile(final String fileName) throws IOException {
        List<String> dataStrings = new LinkedList<>();
        try (Stream<String> streamFromFile = Files.lines(Paths.get(fileName))) {
            streamFromFile.skip(1)
                    .forEach(s -> dataStrings.add(s.replaceAll("\\s+", " ")));
            if (dataStrings.isEmpty()) {
                throw new EmptyFileException();
            }
        } catch (NoSuchFileException exc) {
            LOG.log(Level.FATAL,
                    "File with name \"" + fileName + "\" not found.",
                    exc);
            throw exc;
        } catch (EmptyFileException exc) {
            LOG.log(Level.FATAL, "File is empty.", exc);
            throw exc;
        } catch (IOException exc) {
            LOG.log(Level.FATAL, "Unable to open file.", exc);
            throw exc;
        }
        return dataStrings;
    }
}
