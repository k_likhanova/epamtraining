package by.training.quadrangle.parser;

import by.training.quadrangle.action.Transformation;
import by.training.quadrangle.creator.PointCreator;
import by.training.quadrangle.customexception.IllegalObjectException;
import by.training.quadrangle.entity.Point;
import by.training.quadrangle.validator.FileValidator;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import by.training.quadrangle.validator.RegEx;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The type Parser.
 * Created on 24.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class Parser {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("infoLogger");

    /**
     * Parse file strings list.
     * Lines not corresponding validLine are passed.
     *
     * @param fileStrings the file strings
     * @return the valid strings list
     */
    public List<List<String>> parseFileStrings(final List<String> fileStrings) {
        List<String[]> validFileStrings = new LinkedList<>();
        for (String fileString : fileStrings) {
            if (FileValidator.validateLine(
                                       fileString, RegEx.FILE_STRING_PATTERN)) {
                validFileStrings.add(fileString.trim().split(" "));
            } else {
                LOG.log(Level.INFO, fileString + " is not valid.");
            }
        }
        return Transformation.transformArrayToList(validFileStrings);
    }

    /**
     * Parse file string to double.
     *
     * @param string the string
     * @return the list
     */
    public List<Double> parseStringToDouble(final List<String> string) {
        List<Double>  numbers = new LinkedList<>();
        for (String number : string) {
            if (FileValidator.validateLine(
                                number, RegEx.DOUBLE_STRING_PATTERN)) {
                numbers.add(Double.parseDouble(number));
            } else {
                LOG.log(Level.INFO, number + " is not double.");
            }
        }
        return numbers;
    }

    /**
     * Parse file string to points list.
     *
     * @param fileString the string
     * @return the points list
     * @throws IllegalObjectException the illegal object exception
     */
    public List<Point> parseStringToPoints(final List<String> fileString)
                                                throws IllegalObjectException {
        PointCreator pointCreator = new PointCreator();
        List<Point> points = new LinkedList<>();
        int i = 0;
        while (i < fileString.size() / Point.getCoordinatesAmount()) {
            points.add(pointCreator.create(
                                    fileString.stream()
                                        //skips the passable points
                                        .skip((long) points.size()
                                                * Point.getCoordinatesAmount())
                                        .limit(Point.getCoordinatesAmount())
                                        .collect(Collectors.toList())
            ));
            ++i;
        }
        return points;
    }
}
