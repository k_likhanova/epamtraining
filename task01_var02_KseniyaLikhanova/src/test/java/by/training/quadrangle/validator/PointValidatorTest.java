package by.training.quadrangle.validator;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static java.util.Arrays.asList;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class PointValidatorTest {

    @DataProvider(name = "isNotPoint")
    public Object[][] isNotQuadrangle() {
        return new Object[][]{
                {new LinkedList<>(asList(1.0, 1.0, 1.0))},
                {Collections.singletonList(1.2)}
        };
    }

    @Test
    public void testIsPoint() {
        Assert.assertTrue(PointValidator.isPoint(
                            new LinkedList<>(asList(1.0, 1.0))));
    }

    @Test(dataProvider = "isNotPoint")
    public void testIsNotPoint(final List<Double> coordinates) {
        Assert.assertFalse(PointValidator.isPoint(coordinates));
    }
}