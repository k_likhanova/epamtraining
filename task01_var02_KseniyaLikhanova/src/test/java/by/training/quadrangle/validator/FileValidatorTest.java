package by.training.quadrangle.validator;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class FileValidatorTest {
    @DataProvider(name = "goodData")
    public Object[][] goodData() {
        return new Object[][]{
                {"Ghjk 1 2 3 4 5 6 7 8"},
                {"Jhgf 1.67 2.56 3.67 4.56 5.87 6.97 7.54 8.34"},
                {"Obnm -1.67 -2.56 -3.67 4.56 5.87 -6.97 -7.54 -8.34"},
                {"Guikl   -1.67      -2.56    -3.67   4.56    5.87   -6.97    -7.54   -8.34"},
                {"Kfghjm 123456.1234567 1234.1234 1234.12345 123.234 345.456 4356.4567 5678.6789 345.34"},
                {"Jvbnm 0.67 2.0 0.05 3.06 0.35 8.13 -0.54 -8.34"}
        };
    }

    @DataProvider(name = "badData")
    public Object[][] badData() {
        return new Object[][]{
                {"Gjkl; 1 2 3 4 5 6 7 8 9 10"},
                {"Hhjk 1 2 3 4 5 6 7 8 9"},
                {"hjklh 1 2 3 4 5 6 7 8"},
                {"Hbnmk, 1 2 3 4 5 6 7"},
                {"Okjmnb 1 2 3 4 5 6"},
                {"Ljh 1 2 3 4 --5 --6 --7 --8"},
                {"Jbnm 1 2 3 4 +5.6 +6.5 +7.6 +8.3"},
                {"Kgbhn 1 2 3 4 5.w 6.z 7.x 8.y"},
                {"Kghj 1,2 2,3 3,4 4,5 5,6 6,5 7,6 8,3"},
                {"Khjk 1 2 sadf quadrangle oracle java asdf"}
        };
    }

    @Test(dataProvider = "goodData")
    public void positiveTestValidateLine(final String validateLine) {
        Assert.assertTrue(FileValidator.validateLine(
                validateLine, RegEx.FILE_STRING_PATTERN));
    }

    @Test(dataProvider = "badData")
    public void negativeTestValidateLine(final String validateLine) {
        Assert.assertFalse(FileValidator.validateLine(
                validateLine, RegEx.FILE_STRING_PATTERN));
    }
}