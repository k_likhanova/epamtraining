package by.training.quadrangle.validator;

import by.training.quadrangle.entity.Point;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.LinkedList;
import java.util.List;

import static java.util.Arrays.asList;

public class QuadrangleValidatorTest {
    @DataProvider(name = "isContainsThreePointLine")
    public Object[][] isContainsThreePointLine() {
        return new Object[][]{
            {new LinkedList<>(asList(
                            new Point(0,0), new Point(1,0),
                            new Point(2,0), new Point(2,1)))},
            {new LinkedList<>(asList(
                            new Point(0,0), new Point(0,1),
                            new Point(0,2), new Point(1,2)))},
            {new LinkedList<>(asList(
                            new Point(0,0), new Point(1,1),
                            new Point(2,2), new Point(2,1)))}
        };
    }

    @DataProvider(name = "isNotContainsThreePointLine")
    public Object[][] isNotContainsThreePointLine() {
        return new Object[][]{
            {new LinkedList<>(asList(
                            new Point(0,0), new Point(0,1),
                            new Point(1,1), new Point(1,0)))},
            {new LinkedList<>(asList(
                            new Point(0,0), new Point(1,1),
                            new Point(2,0), new Point(-1,-1)))},
            {new LinkedList<>(asList(
                            new Point(0,1), new Point(1,1),
                            new Point(2,0), new Point(0,0)))}
        };
    }

    @DataProvider(name = "isSimple")
    public Object[][] isSimple() {
        return new Object[][]{
            {new LinkedList<>(asList(
                            new Point(0,0), new Point(0,1),
                            new Point(1,1), new Point(1,0)))},
            {new LinkedList<>(asList(
                            new Point(0,0), new Point(1,1),
                            new Point(2,0), new Point(-1,-1)))},
            {new LinkedList<>(asList(
                            new Point(0,0), new Point(1,1),
                            new Point(2,0), new Point(1,0.5)))},
            {new LinkedList<>(asList(
                            new Point(-1,-1), new Point(0,0),
                            new Point(1,0), new Point(0.5,-0.5)))}
        };
    }

    @DataProvider(name = "isNotSimple")
    public Object[][] isNotSimple() {
        return new Object[][]{
            {new LinkedList<>(asList(
                            new Point(0,0), new Point(0,1),
                            new Point(1,1), new Point(-1,0)))},
            {new LinkedList<>(asList(
                            new Point(0,0), new Point(1,1),
                            new Point(2,0), new Point(2,1)))}
        };
    }

    @DataProvider(name = "isQuadrangle")
    public Object[][] isQuadrangle() {
        return new Object[][]{
            {new LinkedList<>(asList(
                            new Point(0,0), new Point(0,1),
                            new Point(1,1), new Point(1,0)))},
            {new LinkedList<>(asList(
                            new Point(0,0), new Point(1,1),
                            new Point(2,0), new Point(1,0.5)))}
        };
    }

    @DataProvider(name = "isNotQuadrangle")
    public Object[][] isNotQuadrangle() {
        return new Object[][]{
            {new LinkedList<>(asList(new Point(0,0),
                                     new Point(1,1),
                                     new Point(2,0)))},
            {new LinkedList<>(asList(
                            new Point(0,0), new Point(1,1),
                            new Point(1,0), new Point(2,0)))},
            {new LinkedList<>(asList(
                            new Point(0,0), new Point(1,1),
                            new Point(2,0), new Point(2,1)))}
        };
    }

    @Test(dataProvider = "isQuadrangle")
    public void testIsQuadrangle(final List<Point> points) {
        Assert.assertTrue(QuadrangleValidator.isQuadrangle(points));
    }

    @Test(dataProvider = "isNotQuadrangle")
    public void testIsNotQuadrangle(final List<Point> points) {
        Assert.assertFalse(QuadrangleValidator.isQuadrangle(points));
    }

    @Test(dataProvider = "isContainsThreePointLine")
    public void testIsContainsThreePointLine(final List<Point> points) {
        Assert.assertTrue(QuadrangleValidator.isContainsThreePointLine(points));
    }

    @Test(dataProvider = "isNotContainsThreePointLine")
    public void testIsNotContainsThreePointLine(final List<Point> points) {
        Assert.assertFalse(QuadrangleValidator.isContainsThreePointLine(points));
    }

    @Test(dataProvider = "isSimple")
    public void testIsSimple(final List<Point> points) {
        Assert.assertTrue(QuadrangleValidator.isSimple(points));
    }

    @Test(dataProvider = "isNotSimple")
    public void testIsNotSimple(final List<Point> points) {
        Assert.assertFalse(QuadrangleValidator.isSimple(points));
    }
}