package by.training.quadrangle.creator;

import by.training.quadrangle.customexception.IllegalObjectException;
import by.training.quadrangle.entity.Point;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedList;

import static java.util.Arrays.asList;

public class PointCreatorTest {
    private PointCreator creator;

    @BeforeClass
    public void initialize() {
        creator = new PointCreator();
    }

    @Test
    public void testIllegalObjectException() {
        Assert.assertThrows(IllegalObjectException.class,
                            () -> creator.create(
                                new LinkedList<>(asList("1", "1", "1"))));
    }

    @Test
    public void testCreate() throws IllegalObjectException {
        Assert.assertEquals(new Point(1, 1),
                            creator.create(
                                new LinkedList<>(asList("1", "1"))));
    }
}