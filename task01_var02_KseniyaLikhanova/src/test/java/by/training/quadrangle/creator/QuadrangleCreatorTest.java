package by.training.quadrangle.creator;

import by.training.quadrangle.customexception.IllegalObjectException;
import by.training.quadrangle.entity.Point;
import by.training.quadrangle.entity.Quadrangle;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedList;
import java.util.List;

import static java.util.Arrays.asList;

public class QuadrangleCreatorTest {

    private QuadrangleCreator creator;

    @BeforeClass
    public void initialize() {
        creator = new QuadrangleCreator();
    }

    @Test
    public void testIllegalObjectException() {
        Assert.assertThrows(IllegalObjectException.class,
                () -> creator.create(new LinkedList<>(
                        asList("Hsdf", "0", "0", "0", "1", "1", "1", "-1", "1"))));
    }
    @Test
    public void testCreate() throws IllegalObjectException {
        List<Point> points = new LinkedList<>();
        points.add(new Point(0, 0));
        points.add(new Point(0, 1));
        points.add(new Point(1, 1));
        points.add(new Point(1, 0));
        Assert.assertEquals(new Quadrangle("Hsdf", points),
                creator.create(new LinkedList<>(
                        asList("Hsdf", "0", "0", "0", "1", "1", "1", "1", "0"))));
    }
}