package by.training.quadrangle.reader;

import by.training.quadrangle.customexception.EmptyFileException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.LinkedList;
import java.util.List;


public class FileDataReaderTest {
    private static final String FILE_PATH = "data/test/reader/";

    private final FileDataReader reader = new FileDataReader();
    private final List<String> fileStrings = new LinkedList<>();

    @BeforeClass
    public void initFileStrings() {
        fileStrings.add("0 1 1 1 2 3 2 2");
        fileStrings.add("-1 0 4 5 6 7 10 3 ododod javajavajava");
        fileStrings.add("324 234 oracle ww 567 76");
        fileStrings.add("4 5 6 77 8 9 0");
    }

    @AfterClass
    public void deleteFileStrings() {
        fileStrings.clear();
    }

    @Test
    public void testNoSuchFileException() {
        Assert.assertThrows(NoSuchFileException.class,
                () -> reader.readFromFile(FILE_PATH + "abr.txt"));
    }

    @Test
    public void testEmptyFileException() {

        Assert.assertThrows(EmptyFileException.class,
                () -> reader.readFromFile(FILE_PATH + "Empty.txt"));
    }

    @Test
    public void testReadFromFile() throws IOException {
        Assert.assertEquals(reader.readFromFile(FILE_PATH + "DataTest.txt"), fileStrings);
    }
}