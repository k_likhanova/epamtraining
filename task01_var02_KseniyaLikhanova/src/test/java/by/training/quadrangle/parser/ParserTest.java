package by.training.quadrangle.parser;

import by.training.quadrangle.action.Transformation;
import by.training.quadrangle.customexception.IllegalObjectException;
import by.training.quadrangle.entity.Point;
import by.training.quadrangle.reader.FileDataReader;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class ParserTest {
    private static final String FILE_PATH = "data/test/parser/";

    private FileDataReader reader;
    private Parser parser;
    private List<List<String>> expectedFileStrings;
    private double[] numbers1;
    private double[] numbers2;

    @BeforeClass
    public void initField() {
        reader = new FileDataReader();
        parser = new Parser();
        expectedFileStrings = new ArrayList<>();
        expectedFileStrings.add(asList(
                                "Jsdera", "1", "2", "3",
                                "4", "5", "6", "7", "8"));
        expectedFileStrings.add(asList(
                                "J", "14", "0.2124567897654", "0.3",
                                "41.1", "5.23", "6.23", "7.45", "8.0"));
        numbers1 = new double[]{1, 2, 3, 4, 5, 6, 7, 8};
        numbers2 = new double[]{14, 0.2124567897654, 0.3,
                                41.1, 5.23, 6.23, 7.45, 8.0};
    }

    @DataProvider(name = "fileName")
    public Object[][] fileName() {
        return new Object[][]{
                {FILE_PATH + "DataTest.txt"},
                {FILE_PATH + "DataTestWithIncorrectLines.txt"}
        };
    }

    @DataProvider(name = "stringToDouble")
    public Object[][] stringToDouble() {
        return new Object[][]{
                {numbers1, expectedFileStrings.get(0)},
                {numbers2, expectedFileStrings.get(1)
                                .stream().skip(1).collect(Collectors.toList())}
        };
    }

    @Test(dataProvider = "fileName")
    public void testParseFileStrings(final String fileName) throws IOException {

        Assert.assertTrue(expectedFileStrings.containsAll(
                            parser.parseFileStrings(reader.readFromFile(fileName))));
    }

    @Test(dataProvider = "stringToDouble")
    public void testParseStringToDouble(final double[] expected,
                                        final List<String> arrayString) {
        Assert.assertEquals(expected,
                Transformation.transformListToArray(parser.parseStringToDouble(arrayString)));
    }

    @Test
    public void testParseStringToPoints() throws IllegalObjectException {
        List<Point> expected = new LinkedList<>();
        expected.add(new Point(0, 0));
        expected.add(new Point(0, 1));
        expected.add(new Point(1, 1));
        expected.add(new Point(1, 0));
        Assert.assertEquals(expected,
                parser.parseStringToPoints(new LinkedList<>(
                        asList("0", "0", "0", "1", "1", "1", "1", "0"))));
    }

}
