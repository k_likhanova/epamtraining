package by.training.quadrangle.action;

import by.training.quadrangle.creator.QuadrangleCreator;
import by.training.quadrangle.customexception.IllegalObjectException;
import by.training.quadrangle.entity.Quadrangle;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.LinkedList;

import static java.util.Arrays.asList;

public class QuadrangleKindTest {
    private QuadrangleKind kind;
    private QuadrangleCreator creator;
    private Quadrangle convexQuadrangle;
    private Quadrangle concaveQuadrangle;
    private Quadrangle parallelogram;
    private Quadrangle rhombus;
    private Quadrangle square;
    private Quadrangle trapezium1;
    private Quadrangle trapezium2;

    @BeforeClass
    public void initialize() throws IllegalObjectException {
        kind = new QuadrangleKind();
        creator = new QuadrangleCreator();
        convexQuadrangle = creator.create(new LinkedList<>(
                asList("ConvexQuadrangle", "0", "1", "1", "2",
                                           "3", "3", "2", "1")));
        concaveQuadrangle = creator.create(new LinkedList<>(
                asList("Tent", "0", "0", "1", "2",
                               "2", "0", "1", "1")));
        parallelogram = creator.create(new LinkedList<>(
                asList("Parallelogram", "0", "1", "1", "2",
                                        "4", "2", "3", "1")));
        rhombus = creator.create(new LinkedList<>(
                asList("Rhombus", "1", "1", "2", "2.5",
                                  "3", "1", "2", "-0.5")));
        square = creator.create(new LinkedList<>(
                asList("Square", "0", "0", "0", "2",
                                 "2", "2", "2", "0")));
        trapezium1 = creator.create(new LinkedList<>(
                asList("Trapezium1", "0", "1", "1", "2",
                                     "2", "2", "3", "1")));
        trapezium2 = creator.create(new LinkedList<>(
                asList("Trapezium2", "0", "1", "1", "2",
                                     "4", "3", "2", "1")));
    }

    @DataProvider(name = "isConvex")
    public Object[][] isConvex() throws IllegalObjectException {
        return new Object[][]{
                {parallelogram},
                {rhombus},
                {square},
                {trapezium1},
                {trapezium2},
                {convexQuadrangle}
        };
    }

    @DataProvider(name = "isParallelogram")
    public Object[][] isParallelogram() throws IllegalObjectException {
        return new Object[][]{
                {parallelogram},
                {rhombus},
                {square}
        };
    }

    @DataProvider(name = "isNotParallelogram")
    public Object[][] isNotParallelogram() throws IllegalObjectException {
        return new Object[][]{
                {trapezium1},
                {trapezium2},
                {concaveQuadrangle},
                {convexQuadrangle}
        };
    }

    @DataProvider(name = "isRhombus")
    public Object[][] isRhombus() throws IllegalObjectException {
        return new Object[][]{
                {rhombus},
                {square}
        };
    }

    @DataProvider(name = "isNotRhombus")
    public Object[][] isNotRhombus() throws IllegalObjectException {
        return new Object[][]{
                {parallelogram},
                {trapezium1},
                {trapezium2},
                {concaveQuadrangle},
                {convexQuadrangle}
        };
    }

    @DataProvider(name = "isNotSquare")
    public Object[][] isNotSquare() throws IllegalObjectException {
        return new Object[][]{
                {parallelogram},
                {rhombus},
                {trapezium1},
                {trapezium2},
                {concaveQuadrangle},
                {convexQuadrangle}
        };
    }

    @DataProvider(name = "isTrapezium")
    public Object[][] isTrapezium() throws IllegalObjectException {
        return new Object[][]{
                {parallelogram},
                {rhombus},
                {square},
                {trapezium1},
                {trapezium2}
        };
    }

    @DataProvider(name = "isNotTrapezium")
    public Object[][] isNotTrapezium() throws IllegalObjectException {
        return new Object[][]{
                {concaveQuadrangle},
                {convexQuadrangle}
        };
    }

    @Test(dataProvider = "isConvex")
    public void testIsConvex(final Quadrangle quadrangle) {
        Assert.assertTrue(kind.isConvex(quadrangle));
    }

    @Test
    public void testIsConcave() throws IllegalObjectException {
        Assert.assertFalse(kind.isConvex(concaveQuadrangle));
    }


    @Test(dataProvider = "isParallelogram")
    public void testIsParallelogram(final Quadrangle quadrangle) {
        Assert.assertTrue(kind.isParallelogram(quadrangle));
    }

    @Test(dataProvider = "isNotParallelogram")
    public void testIsNotParallelogram(final Quadrangle quadrangle) {
        Assert.assertFalse(kind.isParallelogram(quadrangle));
    }

    @Test(dataProvider = "isRhombus")
    public void testIsEquilateral(final Quadrangle quadrangle) {
        Assert.assertTrue(kind.isEquilateral(quadrangle));
    }

    @Test(dataProvider = "isNotRhombus")
    public void testIsNotEquilateral(final Quadrangle quadrangle) {
        Assert.assertFalse(kind.isEquilateral(quadrangle));
    }

    @Test(dataProvider = "isRhombus")
    public void testIsRhombus(final Quadrangle quadrangle) {
        Assert.assertTrue(kind.isRhombus(quadrangle));
    }

    @Test(dataProvider = "isNotRhombus")
    public void testIsNotRhombus(final Quadrangle quadrangle) {
        Assert.assertFalse(kind.isRhombus(quadrangle));
    }

    @Test
    public void testIsSquare() {
        Assert.assertTrue(kind.isSquare(square));
    }

    @Test(dataProvider = "isNotSquare")
    public void testIsNotSquare(final Quadrangle quadrangle) {
        Assert.assertFalse(kind.isSquare(quadrangle));
    }

    @Test(dataProvider = "isTrapezium")
    public void testIsTrapezium(final Quadrangle quadrangle) {
        Assert.assertTrue(kind.isTrapezium(quadrangle));
    }

    @Test(dataProvider = "isNotTrapezium")
    public void testIsNotTrapezium(final Quadrangle quadrangle) {
        Assert.assertFalse(kind.isTrapezium(quadrangle));
    }
}