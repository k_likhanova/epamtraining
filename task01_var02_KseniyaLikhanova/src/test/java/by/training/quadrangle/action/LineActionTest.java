package by.training.quadrangle.action;

import by.training.quadrangle.entity.Point;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LineActionTest {

    private final LineAction line = new LineAction();

    @DataProvider(name = "lineLength")
    public Object[][] lineLength() {
        return new Object[][]{
                {new Point(-1, 0), new Point(0, 0), 1},
                {new Point(0, 0), new Point(0, 1), 1},
                {new Point(1.0, 1.0), new Point(3.0, 1.0), 2}
        };
    }

    @DataProvider(name = "distance")
    public Object[][] distance() {
        return new Object[][]{
                {new Point(3, 3), new Point(0, 0),
                        new Point(2, 0), 3},
                {new Point(0, 0), new Point(-1, 0),
                        new Point(2, 0), 0},
                {new Point(-5, 0), new Point(-1, 0),
                        new Point(2, 0), 0},
                {new Point(-3, -3), new Point(0, 0),
                        new Point(2, 0), 3}
        };
    }

    @DataProvider(name = "isOnStraightLine")
    public Object[][] isOnStraightLine() {
        return new Object[][]{
                {new Point(0, 0),
                        new Point(1, 0),
                        new Point(2, 0)},
                {new Point(0, 0),
                        new Point(0, 1),
                        new Point(0, 2)},
                {new Point(0, 0),
                        new Point(1, 1),
                        new Point(2, 2)},
                {new Point(0, 0),
                        new Point(-1, -1),
                        new Point(-2, -2)},
                {new Point(0.5, 0),
                        new Point(1.5, 1),
                        new Point(2.5, 2)}
        };
    }

    @DataProvider(name = "isNotOnStraightLine")
    public Object[][] isNotOnStraightLine() {
        return new Object[][]{
                {new Point(0, 0),
                        new Point(1, 0),
                        new Point(1, 1)},
                {new Point(0, 0),
                        new Point(1, 1),
                        new Point(2, 0)},
                {new Point(0, 1),
                        new Point(1, 0),
                        new Point(2, 0)},
                {new Point(0, 0),
                        new Point(-1, 0),
                        new Point(-1, -1)},
                {new Point(0, 0),
                        new Point(-1, 0),
                        new Point(-2, -1)},
                {new Point(0, -1),
                        new Point(-1, 0),
                        new Point(-2, 0)},
                {new Point(0, 0),
                        new Point(1.5, 1),
                        new Point(2.5, 0)}
        };
    }

    @DataProvider(name = "isParallel")
    public Object[][] isParallel() {
        return new Object[][]{
                {new Point(0, 3), new Point(3, 3),
                        new Point(3, 0), new Point(0, 0)},
                {new Point(0, 0), new Point(0, 1),
                    new Point(1, 1), new Point(1, 0)}
        };
    }

    @DataProvider(name = "isIntersect")
    public Object[][] isIntersect() {
        return new Object[][]{
                {new Point(0, 0), new Point(2, 1),
                        new Point(1, 1), new Point(1, 0)},
                {new Point(0, 0), new Point(0, 1),
                        new Point(1, 1), new Point(-1, 0)}
        };
    }

    @DataProvider(name = "pointPosition")
    public Object[][] pointPosition() {
        return new Object[][]{
                {0, new Point(1, 1),
                        new Point(0, 0), new Point(2, 2)},
                {0, new Point(0, 0),
                        new Point(1, 1), new Point(2, 2)},
                {-1, new Point(1, 1),
                        new Point(0, 0), new Point(3, 0)},
                {-1, new Point(0, -4),
                        new Point(0, 0), new Point(-3, 3)},
                {1, new Point(0, 4),
                        new Point(0, 0), new Point(-3, 3)},
                {1, new Point(2, 2),
                        new Point(0, 0), new Point(0, 4)}
        };
    }


    @DataProvider(name = "isEqualSides")
    public Object[][] isEqualSides() {
        return new Object[][]{
                {new Point(0, 0), new Point(0, 1),
                        new Point(1, 1), new Point(1, 0)},
                {new Point(0, 1), new Point(1, 1),
                        new Point(1, 0), new Point(0, 0)},
                {new Point(0, 0), new Point(1, 1),
                        new Point(0, 1), new Point(1, 2)},
                {new Point(-1, -1), new Point(-2, -2),
                        new Point(1, 1), new Point(2, 2)}
        };
    }


    @Test(dataProvider = "lineLength")
    public void testCalculateLineLength(final Point point1,
                                        final Point point2,
                                        double expected) {
        Assert.assertEquals(expected, line.calculateLineLength(point1, point2));
    }

    @Test(dataProvider = "distance")
    public void testCalculateDistance(final Point point1, final Point point2,
                                      final Point point3, final double expected) {
        Assert.assertEquals(expected, line.calculateDistance(
                                            point1, point2, point3));
    }

    @Test(dataProvider = "isOnStraightLine")
    public void testIsOnStraightLine(final Point point1,
                                     final Point point2,
                                     final Point point3) {

        Assert.assertTrue(line.isOnStraightLine(point1, point2, point3));
    }

    @Test(dataProvider = "isNotOnStraightLine")
    public void testIsNotOnStraightLine(final Point point1,
                                        final Point point2,
                                        final Point point3) {
        Assert.assertFalse(line.isOnStraightLine(point1, point2, point3));
    }

    @Test(dataProvider = "isIntersect")
    public void testIsIntersect(final Point point1, final Point point2,
                                final Point point3, final Point point4) {
        Assert.assertTrue(line.isIntersect(point1, point2,
                                           point3, point4));
    }

    @Test(dataProvider = "isParallel")
    public void testIsNotIntersect(final Point point1, final Point point2,
                                   final Point point3, final Point point4) {
        Assert.assertFalse(line.isIntersect(point1, point2,
                                           point3, point4));
    }

    @Test(dataProvider = "isParallel")
    public void testIsParallel(final Point point1, final Point point2,
                               final Point point3, final Point point4) {
        Assert.assertTrue(line.isParallel(point1, point2,
                point3, point4));
    }

    @Test(dataProvider = "isIntersect")
    public void testIsNotParallel(final Point point1, final Point point2,
                                  final Point point3, final Point point4) {
        Assert.assertFalse(line.isParallel(point1, point2,
                point3, point4));
    }

    @Test(dataProvider = "pointPosition")
    public void testPointPositionConcerningLine(final int expected, final Point point,
                                               final Point startPoint, final Point endPoint){
        Assert.assertEquals(expected, line.searchPointPositionConcerningLine
                                            (point, startPoint, endPoint));
    }

    @Test(dataProvider = "isEqualSides")
    public void testIsEqualSides(final Point startPointLine1, final Point endPointLine1,
                                final Point startPointLine2, final Point endPointLine2){
        Assert.assertTrue(line.isEqualSides(startPointLine1, endPointLine1,
                                            startPointLine2, endPointLine2));
    }
}
