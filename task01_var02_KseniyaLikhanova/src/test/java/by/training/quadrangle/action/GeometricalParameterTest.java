package by.training.quadrangle.action;

import by.training.quadrangle.creator.QuadrangleCreator;
import by.training.quadrangle.customexception.IllegalObjectException;
import by.training.quadrangle.entity.Quadrangle;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedList;

import static java.util.Arrays.asList;
import static org.testng.Assert.*;

public class GeometricalParameterTest {
    private GeometricalParameter parameter;
    private QuadrangleCreator creator;
    private Quadrangle square;

    @BeforeClass
    public void initialize() throws IllegalObjectException {
        parameter = new GeometricalParameter();
        creator = new QuadrangleCreator();
        square = creator.create(new LinkedList<>(
                asList("Square", "0", "0", "0", "2",
                        "2", "2", "2", "0")));
    }

    @Test
    public void testCalculatePerimeter() {
        Assert.assertEquals(
                8.0, parameter.calculatePerimeter(square.getVertices()));
    }

    @Test
    public void testCalculateArea() {
        Assert.assertEquals(
                4.0, parameter.calculateArea(square.getVertices()));
    }
}