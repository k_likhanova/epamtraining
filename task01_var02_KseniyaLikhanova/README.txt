Assignment:
    To develop the classes Point and Quadrangle.
    To create methods and tests:
        calculations of the area and perimeter of a figure;
        whether make points a quadrangle; whether the quadrangle is convex;
        whether the quadrangle is a square, a rhombus, a trapezium.
    To store the areas, perimeters of figures in an object of the Registrar class.
    Any change of the figure parameter has to
        cause recalculation of the corresponding values in the Registrar class.
    To save all created objects of figures in an object of the Repository class.
    Using the Repository template, to develop specifications on addition,
        removal and change of objects of a repository.
    To develop specifications on search of objects and groups of objects in a Repository.
    To develop methods of sorting of objects sets. To use the Comparator interface.

For this project applied such patterns as: Factory method, Singleton, Repository, Specification.
The code corresponds CheckStyle, SonarLint, FindBugs.
To launch app required java 8.

Data for creating objects in file data/main/QuadrangleData.txt.
Log4j configuration in file src/main/resources/log4j2.xml
TestNG configuration in file src/main/resources/testng.xml
The necessary libs describes in pom.xml.

Classes FileDataReader, FileValidator,
        PointValidator, QuadrangleValidator,
        LineAction, QuadrangleKind,
        QuadrangleValidator, Parser,
        PointCreator, QuadrangleCreator are covered with tests.
