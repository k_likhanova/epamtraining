package by.training.text.io;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

public class FileDataWriterTest {
    private static final String FILE_PATH = "data/test/io/DataWriterTest.txt";

    private final FileDataReader reader = new FileDataReader();
    private String fileString;

    @BeforeClass
    public void initFileStrings() {
        fileString = "    It has survived - not only (five) centuries. " +
                "But also the leap into 13<<2 electronic typesetting.\r\n" +
                "    It is a (8^5|1&2<<(2|5>>2&71))|1200 established fact. " +
                "That a io will be of a page when looking at its layout.\r\n" +
                "    Bye.\r\n";
    }

    @Test
    public void testWritStringInFile() throws IOException {
        FileDataWriter writer = new FileDataWriter();
        writer.writStringInFile(FILE_PATH, fileString);
        Assert.assertEquals(reader.readFromFile(FILE_PATH), fileString);
    }
}