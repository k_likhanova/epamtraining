package by.training.text.io;

import by.training.text.customexception.EmptyFileException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.NoSuchFileException;

public class FileDataReaderTest {

    private static final String FILE_PATH = "data/test/io/";

    private final FileDataReader reader = new FileDataReader();
    private String fileString;

    @BeforeClass
    public void initFileString() {
        fileString = "    It has survived - not only (five) centuries. " +
                     "But also the leap into 13<<2 electronic typesetting.\r\n" +
                     "    It is a (8^5|1&2<<(2|5>>2&71))|1200 established fact. " +
                     "That a reader will be of a page when looking at its layout.\r\n" +
                     "    Bye.\r\n";
    }

    @Test
    public void testNoSuchFileException() {
        Assert.assertThrows(NoSuchFileException.class,
                () -> reader.readFromFile(FILE_PATH + "abr.txt"));
    }

    @Test
    public void testEmptyFileException() {
        Assert.assertThrows(EmptyFileException.class,
                () -> reader.readFromFile(FILE_PATH + "Empty.txt"));
    }

    @Test
    public void testReadFromFile() throws IOException {
        Assert.assertEquals(reader.readFromFile(FILE_PATH + "DataTest.txt"), fileString);
    }
}