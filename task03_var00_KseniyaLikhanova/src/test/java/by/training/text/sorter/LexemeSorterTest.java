package by.training.text.sorter;

import by.training.text.composite.Component;
import by.training.text.handler.collector.ParserCollector;
import by.training.text.io.FileDataReader;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class LexemeSorterTest {
    private static final String FILE_PATH = "data/test/io/DataTest.txt";

    private ParserCollector configurator;
    private FileDataReader reader;

    @BeforeClass
    public void initialize(){
        configurator = new ParserCollector();
        reader = new FileDataReader();
    }

    @DataProvider(name = "symbol")
    public Object[][] symbol() {
        return new Object[][]{
                {configurator.parseComponents("Electronic."), 'e', 2},
                {configurator.parseComponents("Electronic."), 'c', 2},
                {configurator.parseComponents("Electronic."), 'o', 1},
                {configurator.parseComponents("Electronic!"), '.', 0},
                {configurator.parseComponents("Electronic..."), '.', 3},
        };
    }

    @Test
    public void testSortByConcreteSymbolAmount() throws IOException {
        String part = reader.readFromFile(FILE_PATH);
        String expected = "    centuries. electronic established reader typesetting. " +
                          "(five) be Bye. leap page survived the when - 1213 52 a a a " +
                          "also at But fact. has into is It It its layout. looking not " +
                          "of only That will\r\n";
        Component text = configurator.parseComponents(part);
        Component sortedText = LexemeSorter.sortByConcreteSymbolAmount(text, 'e');
        Assert.assertEquals(sortedText.toString(), expected);
    }

    @Test(dataProvider = "symbol")
    public void testCountSymbolAmount(final Component composite,
                                      final char symbol,
                                      final int expectedSymbolAmount) {
        Assert.assertEquals(LexemeSorter.countSymbolAmount(composite, symbol), expectedSymbolAmount);
    }
}