package by.training.text.sorter;

import by.training.text.composite.Component;
import by.training.text.handler.collector.ParserCollector;
import by.training.text.io.FileDataReader;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

public class WordSorterTest {

    private static final String FILE_PATH = "data/test/io/DataTest.txt";

    private ParserCollector configurator;
    private FileDataReader reader;

    @BeforeClass
    public void initialize(){
        configurator = new ParserCollector();
        reader = new FileDataReader();
    }

    @Test
    public void testSortWordsByLength() throws IOException {
        String part = reader.readFromFile(FILE_PATH);
        String expected = "    - It has not only (five) survived centuries. " +
                "52 But the also leap into electronic typesetting.\r\n" +
                "    a It is 1213 fact. established " +
                "a a be of at its That will page when reader layout. looking\r\n" +
                "    Bye.\r\n";
        Component text = configurator.parseComponents(part);
        Component sortedText = WordSorter.sortWordsByLength(text);
        Assert.assertEquals(sortedText.toString(), expected);
    }
}