package by.training.text.sorter;

import by.training.text.composite.Component;
import by.training.text.handler.collector.ParserCollector;
import by.training.text.handler.collector.TextCollector;
import by.training.text.io.FileDataReader;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;


public class ParagraphSorterTest {

    private static final String FILE_PATH = "data/test/io/DataTest.txt";

    private ParserCollector configurator;
    private TextCollector textCollector;
    private FileDataReader reader;

    @BeforeClass
    public void initialize(){
        configurator = new ParserCollector();
        textCollector = new TextCollector();
        reader = new FileDataReader();
    }

    @Test
    public void testSortByCountOfSentences() throws IOException {
        String part = reader.readFromFile(FILE_PATH);

        Component text = configurator.parseComponents(part);
        String expected = textCollector.collect(text.getChildren().get(2));
        Component sortedText = ParagraphSorter.sortBySentencesAmount(text);
        String actual = textCollector.collect(sortedText.getChildren().get(0));
        Assert.assertEquals(actual, expected);
    }
}