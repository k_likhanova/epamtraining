package by.training.text.handler.parser.parsertype;

import by.training.text.composite.Component;
import by.training.text.composite.Composite;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SentenceParserTest {

private SentenceParser parser;

    @BeforeClass
    public void initialize(){
        parser = new SentenceParser();
        LexemeParser lexemeParser = new LexemeParser();
        WordParser wordParser = new WordParser();

        parser.setSuccessor(lexemeParser);
        lexemeParser.setSuccessor(wordParser);
    }

    @DataProvider(name = "initializer")
    public Object[][] initializer() {
        return new Object[][]{
                {new Composite(Component.ComponentType.TEXT), "Survived"},
                {new Composite(Component.ComponentType.PARAGRAPH), "Survived"},
                {new Composite(Component.ComponentType.LEXEME), "Survived"},
                {new Composite(Component.ComponentType.WORD), "Survived"},
                {null, "Survived"}
        };
    }

    @Test(dataProvider = "initializer")
    public void testIllegalArgumentException(final Component newComponent,
                                             final String part) {
        Assert.assertThrows(IllegalArgumentException.class,
                () -> new SentenceParser().parse(newComponent, part));
    }

    @Test
    public void testParse() {
        Component sentence = new Composite(Component.ComponentType.SENTENCE);
        String part = "It is a (8^5|1&2<<(2|5>>2&71))|1200 established.";
        parser.parse(sentence, part);
        int expectedSize = 5;
        Assert.assertEquals(((Composite) sentence).getChildren().size(), expectedSize);
    }
}