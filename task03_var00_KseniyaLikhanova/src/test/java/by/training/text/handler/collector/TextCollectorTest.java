package by.training.text.handler.collector;

import by.training.text.io.FileDataReader;
import by.training.text.io.FileDataWriter;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

public class TextCollectorTest {
    private static final String FILE_PATH = "data/Text.txt";
    private static final String FILE_PATH_FOR_RECORD = "data/TextAfterCollector.txt";

    private ParserCollector configurator;
    private TextCollector textCollector;
    private FileDataReader reader;
    private FileDataWriter writer;
    private String fileString;

    @BeforeClass
    public void initialize() {
        configurator = new ParserCollector();
        textCollector = new TextCollector();
        reader = new FileDataReader();
        writer = new FileDataWriter();
        fileString = "    It has survived - not only (five) centuries, " +
                     "but also the leap into 52 electronic typesetting, " +
                     "remaining 0 essentially 9 unchanged. " +
                     "It was popularised in the 5 " +
                     "with the release of Letraset sheets containing Lorem Ipsum passages, " +
                     "and more recently with desktop publishing software " +
                     "like Aldus PageMaker including versions of Lorem Ipsum.\r\n" +
                     "    It is a long established fact that a reader will be distracted " +
                     "by the readable content of a page when looking at its layout. " +
                     "The point of using 78 Ipsum " +
                     "is that it has a more-or-less normal distribution of letters, " +
                     "as opposed to using (Content here), content here', " +
                     "making it look like readable English.\r\n" +
                     "    It is a 1202 established fact " +
                     "that a reader will be of a page when looking at its layout...\r\n" +
                     "    Bye.\r\n";
    }

    @Test
    public void testCollect() throws IOException {
        String part = reader.readFromFile(FILE_PATH);
        String text = textCollector.collect(configurator.parseComponents(part));
        writer.writStringInFile(FILE_PATH_FOR_RECORD, text);
        Assert.assertEquals(text, fileString);
    }
}