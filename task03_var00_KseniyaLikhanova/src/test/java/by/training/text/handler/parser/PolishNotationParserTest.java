package by.training.text.handler.parser;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PolishNotationParserTest {

    private PolishNotationParser converter;

    @BeforeClass
    public void initialize() {
        converter = new PolishNotationParser();
    }

    @DataProvider(name = "convertData")
    public Object[][] dataToConvert() {
        return new Object[][]{
                {"13<<2", "13 2 <<"},
                {"3>>5", "3 5 >>"},
                {"~6&9|(3&4)", "6 ~ 9 & 3 4 & |"},
                {"5|(1&2&(3|(4&(1^5|6&47)|3)|2)|1)",
                        "5 1 2 & 3 4 1 5 ^ 6 47 & | & 3 | | 2 | & 1 | |"},
                {"(7^5|1&2<<(2|5>>2&71))|1200",
                        "7 5 ^ 1 2 2 5 2 >> 71 & | << & | 1200 |"}
        };
    }

    @Test(dataProvider = "convertData")
    public void testParse(final String input, final String expected) {
        String actual = converter.parse(input);
        Assert.assertEquals(actual, expected);
    }

}