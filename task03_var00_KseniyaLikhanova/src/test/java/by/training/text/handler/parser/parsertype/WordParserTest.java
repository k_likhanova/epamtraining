package by.training.text.handler.parser.parsertype;

import by.training.text.composite.Component;
import by.training.text.composite.Composite;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class WordParserTest {

    @DataProvider(name = "words")
    public Object[][] fileName() {
        return new Object[][]{
                {Component.ComponentType.WORD, "survived", 8},
                {Component.ComponentType.WORD, "-", 1},
                {Component.ComponentType.PUNCTUATION, "...", 3},
                {Component.ComponentType.PUNCTUATION, "!", 1},
                {Component.ComponentType.EXPRESSION, "13<<2", 5},
                {Component.ComponentType.EXPRESSION, "(8^5|1&2<<(2|5>>2&71))|1200", 27},
        };
    }

    @Test(dataProvider = "words")
    public void testParse(final Component.ComponentType type, final String part, final int expectedSize) {
        Component word = new Composite(type);
        WordParser parser = new WordParser();
        parser.parse(word, part);
        Assert.assertEquals(word.getChildren().size(), expectedSize);
    }
}