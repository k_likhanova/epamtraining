package by.training.text.handler.parser.parsertype;


import by.training.text.composite.Component;
import by.training.text.composite.Composite;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ParagraphParserTest {
    private ParagraphParser parser;

    @BeforeClass
    public void initialize(){
        parser = new ParagraphParser();
        SentenceParser sentenceParser = new SentenceParser();
        LexemeParser lexemeParser = new LexemeParser();
        WordParser wordParser = new WordParser();

        parser.setSuccessor(sentenceParser);
        sentenceParser.setSuccessor(lexemeParser);
        lexemeParser.setSuccessor(wordParser);
    }

    @DataProvider(name = "initializer")
    public Object[][] initializer() {
        return new Object[][]{
                {new Composite(Component.ComponentType.TEXT), "Survived"},
                {new Composite(Component.ComponentType.LEXEME), "Survived"},
                {new Composite(Component.ComponentType.SENTENCE), "Survived"},
                {new Composite(Component.ComponentType.WORD), "Survived"},
                {null, "Survived"}
        };
    }

    @Test(dataProvider = "initializer")
    public void testIllegalArgumentException(final Component newComponent,
                                             final String part) {
        Assert.assertThrows(IllegalArgumentException.class,
                () -> new ParagraphParser().parse(newComponent, part));
    }

    @Test
    public void testParse() {
        Component paragraph = new Composite(Component.ComponentType.PARAGRAPH);
        String part = "It has survived - not only (five) centuries. But also the leap into. " +
                      "13<<2 electronic typesetting! It is a (8^5|1&2<<(2|5>>2&71))|1200? " +
                      "Established fact that a io will be of a page when looking at its layout...";
        parser.parse(paragraph, part);
        int expectedSize = 5;
        Assert.assertEquals(paragraph.getChildren().size(), expectedSize);
    }
}