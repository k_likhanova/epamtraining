package by.training.text.handler.parser.parsertype;

import by.training.text.composite.Component;
import by.training.text.composite.Composite;
import by.training.text.handler.collector.ParserCollector;
import by.training.text.io.FileDataReader;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TextParserTest {

    private static final String FILE_PATH = "data/test/io/DataTest.txt";

    private ParserCollector configurator;
    private FileDataReader reader;

    @BeforeClass
    public void initialize(){
        configurator = new ParserCollector();
        reader = new FileDataReader();
    }

    @DataProvider(name = "initializer")
    public Object[][] initializer() {
        return new Object[][]{
                {new Composite(Component.ComponentType.PARAGRAPH), "Survived"},
                {new Composite(Component.ComponentType.SENTENCE), "Survived"},
                {new Composite(Component.ComponentType.LEXEME), "Survived"},
                {new Composite(Component.ComponentType.WORD), "Survived"},
                {null, "Survived"}
        };
    }

    @Test(dataProvider = "initializer")
    public void testIllegalArgumentException(final Component newComponent,
                                             final String part) {
        Assert.assertThrows(IllegalArgumentException.class,
                () -> new TextParser().parse(newComponent, part));
    }

    @Test
    public void testParse() {
        String part = "\tIt has survived - not only (five) centuries, " +
                      "but also the leap into 13<<2 electronic typesetting.\n" +
                      "\tIt is a (8^5|1&2<<(2|5>>2&71))|1200 established fact " +
                      "that a io will be of a page when looking at its layout.\n" +
                      "\tBye.\n";
        Component text = configurator.parseComponents(part);
        int expectedSize = 3;
        Assert.assertEquals(text.getChildren().size(), expectedSize);
    }

    @Test
    public void testParseFromDoc() throws IOException {
        String part = reader.readFromFile(FILE_PATH);
        Component text = configurator.parseComponents(part);
        int expectedSize = 3;
        Assert.assertEquals(text.getChildren().size(), expectedSize);
    }

}