package by.training.text.handler.parser.parsertype;

import by.training.text.composite.Component;
import by.training.text.composite.Composite;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LexemeParserTest {

    @DataProvider(name = "initializer")
    public Object[][] initializer() {
        return new Object[][]{
                {new Composite(Component.ComponentType.TEXT), "Survived"},
                {new Composite(Component.ComponentType.PARAGRAPH), "Survived"},
                {new Composite(Component.ComponentType.SENTENCE), "Survived"},
                {new Composite(Component.ComponentType.WORD), "Survived"},
                {null, "Survived"}
        };
    }

    @DataProvider(name = "lexemes")
    public Object[][] fileName() {
        return new Object[][]{
                {Component.ComponentType.WORD, "survived"},
                {Component.ComponentType.WORD, "-"},
                {Component.ComponentType.PUNCTUATION, "..."},
                {Component.ComponentType.PUNCTUATION, "!"},
                {Component.ComponentType.EXPRESSION, "13<<2"},
                {Component.ComponentType.EXPRESSION, "(8^5|1&2<<(2|5>>2&71))|1200"},
        };
    }

    @Test(dataProvider = "initializer")
    public void testIllegalArgumentException(final Component newComponent,
                                             final String part) {
        Assert.assertThrows(IllegalArgumentException.class,
                                    () -> new LexemeParser().parse(newComponent, part));
    }

    @Test(dataProvider = "lexemes")
    public void testParse(final Component.ComponentType expectedType,
                          final String part) {
        Component lexeme = new Composite(Component.ComponentType.LEXEME);
        LexemeParser parser = new LexemeParser();
        parser.parse(lexeme, part);
        Assert.assertEquals(lexeme.getChildren().get(0).getType(), expectedType);
    }

}