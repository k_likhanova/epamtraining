package by.training.text.interpreter;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PolishNotationTest {

    private Client expression;

    @BeforeClass
    public void initialize() {
        expression = new Client();
    }

    @DataProvider(name = "polish")
    public Object[][] dataForPolishNotation() {
        return new Object[][]{
                {"13 2 <<", 52},
                {"3 5 >>", 0},
                {"6 ~ 9 &", 9},
                {"3 4 &", 0},
                {"6 ~ 9 & 3 4 & |", 9},
                {"1 5 ^ 6 47 & |", 6},
                {"5 1 2 & 3 4 1 5 ^ 6 47 & | & 3 | | 2 | & 1 | |", 5},
                {"7 5 ^ 1 2 2 5 2 >> 71 & | << & | 1200 |", 1202},
        };
    }

    @Test(dataProvider = "polish")
    public void testCalculate(final String input, final int expected) {
        expression.parse(input);
        Assert.assertEquals(expression.calculate(), expected);

    }
}