/**
 * Provides the class necessary to calculate expressions.
 * Pattern Interpreter is used.
 */
package by.training.text.interpreter;
