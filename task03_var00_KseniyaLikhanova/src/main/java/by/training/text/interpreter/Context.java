package by.training.text.interpreter;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * The context class contains the initial numerical values of the expression,
 *                            the results of intermediate calculations and
 *                            the final result.
 * Created on 24.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class Context {
    /**
     * Field deque stores the original numerical values of the expression,
     *                      the results of intermediate calculations and
     *                      the final result.
     */
    private Deque<Integer> deque = new ArrayDeque<>();

    /**
     * Push value.
     *
     * @param value the numerical value of the expression
     */
    public void push(final int value) {
        deque.push(value);
    }

    /**
     * Pop value.
     *
     * @return the numerical value of the expression
     */
    public int pop() {
        return deque.pop();
    }
}
