package by.training.text.interpreter;

import java.util.ArrayList;

/**
 * Implementation of syntactic logic of Interpreter and start of process.
 * Created on 24.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class Client {
    /**
     * Field expressions specifies the list with math expression.
     */
    private ArrayList<MathExpression> expressions;
    /**
     * Field context stores the original numerical values of the expression,
     * the results of intermediate calculations and
     * the final result.
     */
    private Context context;

    /**
     * Instantiates a new Client.
     */
    public Client() {
        expressions = new ArrayList<>();
        context = new Context();
    }

    /**
     * Expression parse.
     *
     * @param expression the expression
     */
    public void parse(final String expression) {
        for (String lexeme : expression.split("\\s+")) {
            if (Character.isDigit(lexeme.charAt(0))) {
                expressions.add(c -> c.push(Integer.parseInt(lexeme)));
            } else {
                for (MathOperation operation : MathOperation.values()) {
                    if (operation.getOperation().equals(lexeme)) {
                        switch (operation) {
                            case NOT:
                                expressions.add(c -> c.push(~c.pop()));
                                break;
                            case AND:
                                expressions.add(c -> c.push(c.pop() & c.pop()));
                                break;
                            case OR:
                                expressions.add(c -> c.push(c.pop() | c.pop()));
                                break;
                            case XOR:
                                expressions.add(c -> c.push(c.pop() ^ c.pop()));
                                break;
                            case LEFT_SHIFT:
                                expressions.add(c -> {
                                    int first = c.pop();
                                    int second = c.pop();
                                    c.push(second << first);
                                });
                                break;
                            case RIGHT_SHIFT:
                                expressions.add(c -> {
                                    int first = c.pop();
                                    int second = c.pop();
                                    c.push(second >> first);
                                });
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }

    /**
     * Calculates a complete solution
     * based on the execution of elementary tasks.
     *
     * @return calculated value
     */
    public int calculate() {
        expressions.forEach(c -> c.accept(context));
        return context.pop();
    }
}
