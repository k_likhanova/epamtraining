package by.training.text.interpreter;

/**
 * The enum Math operation.
 * Created on 24.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public enum MathOperation {
    /**
     * Or math operation with priority 1.
     */
    OR("|", 1),
    /**
     * Xor math operation with priority 2.
     */
    XOR("^", 2),
    /**
     * And math operation with priority 3.
     */
    AND("&", 3),
    /**
     * Left shift math operation with priority 4.
     */
    LEFT_SHIFT("<<", 4),
    /**
     * Right shift math operation with priority 4.
     */
    RIGHT_SHIFT(">>", 4),
    /**
     * Not math operation with priority 5.
     */
    NOT("~", 5),
    /**
     * Left bracket math operation with priority 0.
     */
    LEFT_BRACKET("(", 0),
    /**
     * Right bracket math operation with priority 0.
     */
    RIGHT_BRACKET(")", 0);

    /**
     * Field operation specifies the math operation.
     */
    private String operation;
    /**
     * Field priority specifies the priority of the math operation.
     */
    private int priority;

    /**
     * Instantiates a MathOperation.
     *
     * @param mathOperation     the math operation
     * @param priorityOperation the priority of the math operation
     */
    MathOperation(final String mathOperation, final int priorityOperation) {
        this.operation = mathOperation;
        this.priority = priorityOperation;
    }

    /**
     * Gets operation.
     *
     * @return the operation
     */
    public String getOperation() {
        return operation;
    }

    /**
     * Gets priority.
     *
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }
}
