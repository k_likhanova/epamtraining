package by.training.text.composite;

import java.util.List;

/**
 * The interface for any element in Composite pattern.
 * Created on 20.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public interface Component {
    /**
     * The enum Component type.
     */
    enum ComponentType {
        /**
         * Text component type.
         */
        TEXT,
        /**
         * Paragraph component type.
         */
        PARAGRAPH,
        /**
         * Sentence component type.
         */
        SENTENCE,
        /**
         * Lexeme component type.
         */
        LEXEME,
        /**
         * Word component type.
         */
        WORD,
        /**
         * Expression component type.
         */
        EXPRESSION,
        /**
         * Punctuation component type.
         */
        PUNCTUATION,
        /**
         * Symbol component type.
         */
        SYMBOL
    }

    /**
     * Gets children in composite.
     *
     * @return the children in composite
     */
    List<Component> getChildren();

    /**
     * Gets type of composite.
     *
     * @return the type of composite
     */
    ComponentType getType();

    /**
     * Determines if there are children in the composite.
     *
     * @return the boolean
     */
    boolean hasChildren();

    /**
     * Add the component.
     *
     * @param component the component
     * @return the boolean
     */
    boolean add(Component component);

    /**
     * Remove the component.
     *
     * @param component the component
     * @return the boolean
     */
    boolean remove(Component component);

    /**
     * Transforms the component to string.
     *
     * @return string
     */
    String toString();
}
