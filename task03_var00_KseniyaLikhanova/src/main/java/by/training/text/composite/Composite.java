package by.training.text.composite;

import by.training.text.handler.collector.TextCollector;

import java.util.LinkedList;
import java.util.List;

/**
 * The type Composite.
 */
public class Composite implements Component {
    /**
     * Field type specifies the type of composite.
     */
    private ComponentType type;
    /**
     * Field children specifies the children of composite.
     */
    private List<Component> children;

    /**
     * Instantiates a new Composite.
     *
     * @param newType the new type
     */
    public Composite(final ComponentType newType) {
        this.type = newType;
        this.children = new LinkedList<>();
    }

    /**
     * Sets children.
     *
     * @param newComponents the new children
     */
    public void setChildren(final List<Component> newComponents) {
        this.children = newComponents;
    }

    /**
     * Gets children in the composite.
     *
     * @return the children in the composite
     */
    @Override
    public List<Component> getChildren() {
        return children;
    }

    /**
     * Gets type of composite.
     *
     * @return the type of composite
     */
    @Override
    public ComponentType getType() {
        return type;
    }

    /**
     * Determines if there are children in the composite.
     *
     * @return the boolean
     */
    @Override
    public boolean hasChildren() {
        return true;
    }

    /**
     * Add the component.
     *
     * @param component the component
     * @return the boolean
     */
    @Override
    public boolean add(final Component component) {
        return children.add(component);
    }

    /**
     * Remove the component.
     *
     * @param component the component
     * @return the boolean
     */
    @Override
    public boolean remove(final Component component) {
        return children.remove(component);
    }

    /**
     * Transforms the component to string.
     *
     * @return string
     */
    @Override
    public String toString() {
        TextCollector textCollector = new TextCollector();
        return textCollector.collect(this);
    }
}
