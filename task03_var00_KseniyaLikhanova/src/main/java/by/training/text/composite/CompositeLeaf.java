package by.training.text.composite;


import java.util.LinkedList;
import java.util.List;

/**
 * End element of Composite pattern.
 * Created on 20.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class CompositeLeaf implements Component {
    /**
     * Field type specifies the type of composite.
     */
    private ComponentType type;
    /**
     * Field symbol specifies the component of composite leaf.
     */
    private char symbol;

    /**
     * Instantiates a new Composite leaf.
     *
     * @param newSymbol the new symbol
     * @param newType   the new type
     */
    public CompositeLeaf(final char newSymbol, final ComponentType newType) {
        this.type = newType;
        this.symbol = newSymbol;
    }

    /**
     * Sets symbol.
     *
     * @param newSymbol the new symbol
     */
    public void setSymbol(final char newSymbol) {
        this.symbol = newSymbol;
    }

    /**
     * Gets children in the composite.
     *
     * @return the children in the composite
     */
    @Override
    public List<Component> getChildren() {
        return new LinkedList<>();
    }

    /**
     * Gets type of composite.
     *
     * @return the type of composite
     */
    @Override
    public ComponentType getType() {
        return type;
    }

    /**
     * Determines if there are children in the composite.
     *
     * @return the boolean
     */
    @Override
    public boolean hasChildren() {
        return false;
    }

    /**
     * Add the component.
     *
     * @param component the component
     * @return the boolean
     */
    @Override
    public boolean add(final Component component) {
        return false;
    }

    /**
     * Remove the component.
     *
     * @param component the component
     * @return the boolean
     */
    @Override
    public boolean remove(final Component component) {
        return false;
    }

    /**
     * Transforms the component to string.
     *
     * @return string
     */
    @Override
    public String toString() {
        return String.valueOf(symbol);
    }
}
