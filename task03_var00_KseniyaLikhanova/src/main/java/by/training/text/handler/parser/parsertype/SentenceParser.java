package by.training.text.handler.parser.parsertype;

import by.training.text.composite.Component;
import by.training.text.handler.parser.DefaultDataParser;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * Class for sentence parsing on lexemes.
 * Created on 21.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class SentenceParser extends DefaultDataParser {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * Constant field LEXEME_REGEX specifies
     *      the regular expression for lexeme.
     */
    private static final String LEXEME_REGEX = "\\s";
    /**
     * Parse current part of text to node of Composite tree.
     *
     * @param sentence the sentence for parse to lexemes
     * @param part     part of text for current parsing
     */
    @Override
    public void parse(final Component sentence, final String part) {
        if (sentence != null
                && sentence.getType() == Component.ComponentType.SENTENCE
                && sentence.hasChildren()) {
            List<String> lexemes = new LinkedList<>(asList(
                                    part.replaceAll(LEXEME_REGEX + "+", " ")
                                        .split(LEXEME_REGEX)));
            super.addChildren(sentence, lexemes,
                              Component.ComponentType.LEXEME);
        } else {
            LOG.log(Level.INFO, "This composite isn't sentence "
                                            + "or hasn't children.");
            throw new IllegalArgumentException("This composite isn't sentence "
                                                       + "or hasn't children.");
        }
    }
}

