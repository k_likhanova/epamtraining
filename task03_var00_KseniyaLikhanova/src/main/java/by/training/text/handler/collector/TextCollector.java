package by.training.text.handler.collector;

import by.training.text.composite.Component;
import by.training.text.composite.CompositeLeaf;
import by.training.text.handler.parser.PolishNotationParser;
import by.training.text.interpreter.Client;

/**
 * Class for transform the component to string.
 * Created on 25.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class TextCollector {
    /**
     * Transforms the component to string.
     *
     * @param composite the composite
     * @return the string
     */
    public String collect(final Component composite) {
        StringBuilder resultString = new StringBuilder();
        for (Component child : composite.getChildren()) {
            switch (child.getType()) {
                case TEXT:
                    resultString.append(collect(child));
                    break;
                case PARAGRAPH:
                    resultString.append("    ".concat(collect(child)));
                    if (resultString.length() != 0) {
                        resultString.deleteCharAt(resultString.length() - 1);
                    }
                    resultString.append("\r\n");
                    break;
                case SENTENCE:
                    resultString.append(collect(child));
                    break;
                case LEXEME:
                    resultString.append(collect(child));
                    break;
                case WORD:
                    resultString.append(collect(child).concat(" "));
                    break;
                case EXPRESSION:
                    Client expression = new Client();
                    expression.parse(new PolishNotationParser()
                                            .parse(collect(child)));
                    resultString.append(expression.calculate())
                                .append(" ");
                    break;
                case PUNCTUATION:
                    if (resultString.length() != 0) {
                        resultString.deleteCharAt(resultString.length() - 1);
                    }
                    resultString.append(collect(child).concat(" "));
                    break;
                case SYMBOL:
                    CompositeLeaf leaf = (CompositeLeaf) child;
                    resultString.append(leaf.toString());
                    break;
                default:
                    break;
            }
        }
        return resultString.toString();
    }
}
