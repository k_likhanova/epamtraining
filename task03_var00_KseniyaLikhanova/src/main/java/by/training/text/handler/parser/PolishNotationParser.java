package by.training.text.handler.parser;

import by.training.text.interpreter.MathOperation;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;
import java.util.TreeMap;

/**
 * Class for parse expression from string to polish notation.
 * Created on 24.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class PolishNotationParser {

    /**
     * Constant field SPACE specifies the symbol space.
     */
    private static final String SPACE = " ";
    /**
     * Constant field LEFT_SHIFT_SYMBOL specifies the left shift symbol.
     */
    private static final char LEFT_SHIFT_SYMBOL = '<';
    /**
     * Constant field RIGHT_SHIFT_SYMBOL specifies the right shift symbol.
     */
    private static final char RIGHT_SHIFT_SYMBOL = '>';
    /**
     * Field operationMap specifies the map with key = operation and
     * value = priority of operation.
     */
    private Map<String, Integer> operationMap;

    /**
     * Instantiates a new Polish notation parser.
     */
    public PolishNotationParser() {
        operationMap = new TreeMap<>();
        for (MathOperation operation : MathOperation.values()) {
            operationMap.put(operation.getOperation(), operation.getPriority());
        }
    }

    /**
     * Parse expression from string to polish notation.
     *
     * @param expression the expression
     * @return the polish notation
     */
    public String parse(final String expression) {
        Deque<String> operationsDeque = new ArrayDeque<>();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < expression.length(); i++) {
            if (Character.isDigit(expression.charAt(i))) {
                i = addDigits(expression, result, i);
            } else {
                String operation;
                switch (expression.charAt(i)) {
                    case LEFT_SHIFT_SYMBOL:
                        operation = MathOperation.LEFT_SHIFT.getOperation();
                        i++;
                        break;
                    case RIGHT_SHIFT_SYMBOL:
                        operation = MathOperation.RIGHT_SHIFT.getOperation();
                        i++;
                        break;
                    default:
                        operation = String.valueOf(expression.charAt(i));
                        break;
                }
                addOperation(operation, result, operationsDeque);
            }
        }
        while (!operationsDeque.isEmpty()) {
            result.append(operationsDeque.pop());
            result.append(SPACE);
        }
        return result.toString().trim();
    }

    /**
     * Adds digits to polish notation.
     *
     * @param expression the expression
     * @param result     the polish notation
     * @param index      the index of iterations of a basis cycle
     * @return the index
     */
    private int addDigits(final String expression,
                          final StringBuilder result,
                          final int index) {
        int i = index;
        int startIndex = i;
        int current = startIndex;
        while (i < expression.length()
                && Character.isDigit(expression.charAt(current))) {
            current++;
            i++;
        }
        i--;
        result.append(expression.substring(startIndex, current))
              .append(SPACE);
        return i;
    }

    /**
     * Adds operation to polish notation.
     *
     * @param operation       the operation
     * @param result          the polish notation
     * @param operationsDeque the deque with operations of expression
     */
    private void addOperation(final String operation,
                              final StringBuilder result,
                              final Deque<String> operationsDeque) {
        if (operation.equals(MathOperation.RIGHT_BRACKET.getOperation())) {
            String peek = operationsDeque.pop();
            while (!peek.equals(MathOperation.LEFT_BRACKET.getOperation())) {
                result.append(peek);
                result.append(SPACE);
                peek = operationsDeque.pop();
            }
        } else if (!operationsDeque.isEmpty()
                && !operation.equals(MathOperation.LEFT_BRACKET.getOperation())
                && operationMap.get(operation)
                    <= operationMap.get(operationsDeque.peek())) {
            do {
                result.append(operationsDeque.pop());
                result.append(SPACE);
            }
            while (!operationsDeque.isEmpty()
                    && operationMap.get(operation)
                        <= operationMap.get(operationsDeque.peek()));
            operationsDeque.push(operation);
        } else {
            operationsDeque.push(operation);
        }
    }
}
