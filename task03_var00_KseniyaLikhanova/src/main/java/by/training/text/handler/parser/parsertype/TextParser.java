package by.training.text.handler.parser.parsertype;

import by.training.text.composite.Component;
import by.training.text.handler.parser.DefaultDataParser;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * Class for text parsing on paragraphs.
 * Created on 21.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class TextParser extends DefaultDataParser {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");

    /**
     * Constant field PARAGRAPH_EXPRESSION specifies
     *      the regular expression for paragraph.
     */
    private static final String PARAGRAPH_EXPRESSION = "\\n|(\\r\\n)";
    /**
     * Parse current part of text to node of Composite tree.
     *
     * @param text the text for parse to paragraphs
     * @param part part of text for current parsing
     */
    @Override
    public void parse(final Component text, final String part) {
        if (text != null
                && text.getType() == Component.ComponentType.TEXT
                && text.hasChildren()) {
            List<String> paragraphs = new LinkedList<>(asList(
                                        part.split(PARAGRAPH_EXPRESSION)));
            super.addChildren(text, paragraphs,
                              Component.ComponentType.PARAGRAPH);
        } else {
            LOG.log(Level.INFO, "This composite isn't text "
                                        + "or hasn't children.");
            throw new IllegalArgumentException("This composite isn't text "
                                                    + "or hasn't children.");
        }
    }
}
