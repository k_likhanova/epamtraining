package by.training.text.handler.parser.parsertype;

import by.training.text.composite.Component;
import by.training.text.handler.parser.DefaultDataParser;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Class for lexemes parsing on word, expression, punctuation.
 * Created on 21.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class LexemeParser extends DefaultDataParser {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * Constant field PUNCTUATION_REGEX specifies
     *      the regular expression for punctuation.
     */
    private static final String PUNCTUATION_REGEX
                                = "([,:;\\.\\?!]{1}|\\.{3})";
    /**
     * Constant field EXPRESSION_REGEX specifies
     *      the regular expression for expression.
     */
    private static final String EXPRESSION_REGEX
                                    = "((\\(+)?[\\d|&<{2}>{2}~^](\\)+)?)+";
    /**
     * Constant field WORD_REGEX specifies
     *      the regular expression for word.
     */
    private static final String WORD_REGEX
                                    = "(\\(|\\\")?[a-zA-Z-']+(\\)|\\\")?";
    /**
     * Parse current part of text to node of Composite tree.
     *
     * @param lexeme the lexeme for parse to word, expression, punctuation
     * @param part      part of text for current parsing
     */
    @Override
    public void parse(final Component lexeme, final String part) {
        if (lexeme != null
                && lexeme.getType() == Component.ComponentType.LEXEME
                && lexeme.hasChildren()) {
            List<String> words = super.parseRegular(part, WORD_REGEX);
            List<String> expressions = super.parseRegular(part,
                                                            EXPRESSION_REGEX);
            List<String> punctuations = super.parseRegular(part,
                                                            PUNCTUATION_REGEX);

            super.addChildren(lexeme, words, Component.ComponentType.WORD);
            super.addChildren(lexeme, expressions,
                              Component.ComponentType.EXPRESSION);
            super.addChildren(lexeme, punctuations,
                              Component.ComponentType.PUNCTUATION);
        } else {
            LOG.log(Level.INFO, "This composite isn't lexeme "
                                          + "or hasn't children.");
            throw new IllegalArgumentException("This composite isn't lexeme "
                                                      + "or hasn't children.");
        }
    }
}
