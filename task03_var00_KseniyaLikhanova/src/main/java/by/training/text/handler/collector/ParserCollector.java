package by.training.text.handler.collector;

import by.training.text.composite.Component;
import by.training.text.composite.Composite;
import by.training.text.handler.parser.parsertype.*;

/**
 * Class for configurations of parsers chain.
 * Created on 21.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class ParserCollector {
    /**
     * Fields textParser specifies the object of TextParser.
     */
    private TextParser textParser;

    /**
     * Instantiates a new Parser configurator.
     * Creates subjects of parsers
     *      and makes a chain of responsibility.
     */
    public ParserCollector() {
        textParser = new TextParser();
        ParagraphParser paragraphParser = new ParagraphParser();
        SentenceParser sentenceParser = new SentenceParser();
        LexemeParser lexemeParser = new LexemeParser();
        WordParser wordParser = new WordParser();

        textParser.setSuccessor(paragraphParser);
        paragraphParser.setSuccessor(sentenceParser);
        sentenceParser.setSuccessor(lexemeParser);
        lexemeParser.setSuccessor(wordParser);
    }

    /**
     * Parse current part of text to node of Composite tree.
     *
     * @param part the part of text for parsing
     * @return component
     */
    public Component parseComponents(final String part) {
        Component text = new Composite(Component.ComponentType.TEXT);
        textParser.parse(text, part);
        return text;
    }
}
