package by.training.text.handler.parser;

import by.training.text.composite.Component;

/**
 * Interface for all Handlers, used for parsing.
 * Created on 20.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public interface DataParser {
    /**
     * Sets next node in Chain of Responsibility pattern.
     *
     * @param successor the next parser
     */
    void setSuccessor(DataParser successor);

    /**
     * Parse current part of text to node of Composite tree.
     *
     * @param component the component for parsing
     * @param part part of text for current parsing
     */
    void parse(Component component, String part);
}
