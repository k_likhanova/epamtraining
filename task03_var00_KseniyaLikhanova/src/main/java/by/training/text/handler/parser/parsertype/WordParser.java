package by.training.text.handler.parser.parsertype;

import by.training.text.composite.Component;
import by.training.text.composite.CompositeLeaf;
import by.training.text.handler.parser.DefaultDataParser;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class for word parsing on characters.
 * Created on 21.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class WordParser extends DefaultDataParser {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");

    /**
     * Parse current part of text to node of Composite tree.
     *
     * @param word the word for parse to characters
     * @param part part of text for current parsing
     */
    @Override
    public void parse(final Component word, final String part) {
        char[] characters = part.toCharArray();
        for (char ch : characters) {
            LOG.log(Level.DEBUG, "Composite with SYMBOL type "
                                    + "was created. \"" + ch + "\"");
            word.add(new CompositeLeaf(ch, Component.ComponentType.SYMBOL));
        }
    }
}
