/**
 * Provides the class necessary to parse the text
 *      extends from DefaultDataParser.
 * Pattern Chain of Responsibility is used.
 */
package by.training.text.handler.parser.parsertype;
