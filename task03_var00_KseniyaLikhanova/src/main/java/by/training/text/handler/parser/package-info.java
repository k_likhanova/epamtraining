/**
 * Provides the class necessary to parse.
 * Pattern Chain of Responsibility is used.
 */
package by.training.text.handler.parser;
