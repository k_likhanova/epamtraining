package by.training.text.handler.parser.parsertype;

import by.training.text.composite.Component;
import by.training.text.handler.parser.DefaultDataParser;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Class for paragraph parsing on sentences.
 * Created on 21.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class ParagraphParser extends DefaultDataParser {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * Constant field SENTENCE_REGEX specifies
     *      the regular expression for sentence.
     */
    private static final String SENTENCE_REGEX
                        = "[A-Z(~1-9][^\\.\\?\\!]*(\\.{3}|[\\.\\?\\!]{1})";

    /**
     * Parse current part of text to node of Composite tree.
     *
     * @param paragraph the paragraph for parse to sentences
     * @param part      part of text for current parsing
     */
    @Override
    public void parse(final Component paragraph, final String part) {
        if (paragraph != null
                && paragraph.getType() == Component.ComponentType.PARAGRAPH
                && paragraph.hasChildren()) {
            List<String> sentences = super.parseRegular(part, SENTENCE_REGEX);
            super.addChildren(paragraph, sentences,
                                            Component.ComponentType.SENTENCE);
        } else {
            LOG.log(Level.INFO, "This composite isn't paragraph "
                                            + "or hasn't children.");
            throw new IllegalArgumentException("This composite isn't paragraph "
                                                       + "or hasn't children.");
        }
    }
}
