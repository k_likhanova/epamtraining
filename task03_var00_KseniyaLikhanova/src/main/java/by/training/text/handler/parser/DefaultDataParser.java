package by.training.text.handler.parser;


import by.training.text.composite.Component;
import by.training.text.composite.Composite;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Abstract class for all Handlers, used for parsing.
 * Created on 20.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public abstract class DefaultDataParser implements DataParser {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * Field type specifies the next node in Chain of Responsibility pattern.
     */
    private DataParser successor;

    /**
     * Sets next node in Chain of Responsibility pattern.
     *
     * @param newSuccessor next parser
     */
    @Override
    public void setSuccessor(final DataParser newSuccessor) {
        this.successor = newSuccessor;
    }

    /**
     * Gets successor.
     *
     * @return the successor
     */
    public DataParser getSuccessor() {
        return successor;
    }

    /**
     * Parse current part of text to node of Composite tree.
     *
     * @param component the component for parsing
     * @param part      part of text for current parsing
     */
    @Override
    public void parse(final Component component, final String part) {
    }

    /**
     * Parses input string by input expression to List.
     *
     * @param text       string for parsing
     * @param expression regular expression for parsing
     * @return list of all matches in input string
     */
    public List<String> parseRegular(final String text,
                                     final String expression) {
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(text);
        List<String> list = new LinkedList<>();
        while (matcher.find()) {
            list.add(text.substring(matcher.start(), matcher.end()));
        }
        return list;
    }

    /**
     * Add children in component.
     *
     * @param component the component
     * @param children  the list of children
     * @param type      the type of component
     */
    public void addChildren(final Component component,
                             final List<String> children,
                             final Component.ComponentType type) {
        for (String child : children) {
            Component current = new Composite(type);
            LOG.log(Level.DEBUG, "Composite with " + type + " type "
                    + "was created. \"" + child + "\"");
            component.add(current);
            if (getSuccessor() != null) {
                getSuccessor().parse(current, child);
            } else {
                LOG.log(Level.INFO, "Successor is not found.");
            }
        }
    }
}
