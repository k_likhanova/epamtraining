package by.training.text.io;

import by.training.text.customexception.EmptyFileException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;

/**
 * The type File data io.
 * Created on 20.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class FileDataReader {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");

    /**
     * Read from file list of strings.
     *
     * @param fileName the file name
     * @return the text
     * @throws IOException the io exception
     */
    public String readFromFile(final String fileName) throws IOException {
        File file = new File(fileName);
        String content;
        try {
            content = new String(Files.readAllBytes(Paths.get(fileName)),
                                StandardCharsets.UTF_8.name());
            if (file.length() == 0) {
                throw new EmptyFileException();
            }
        } catch (NoSuchFileException exc) {
            LOG.log(Level.FATAL,
                    "File with name \"" + fileName + "\" not found.",
                    exc);
            throw exc;
        } catch (EmptyFileException exc) {
            LOG.log(Level.FATAL, "File is empty.", exc);
            throw exc;
        } catch (IOException exc) {
            LOG.log(Level.FATAL, "Unable to open file.", exc);
            throw exc;
        }
        return content;
    }
}
