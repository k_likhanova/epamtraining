/**
 * Provides the class necessary to read data from file or write in file.
 */
package by.training.text.io;
