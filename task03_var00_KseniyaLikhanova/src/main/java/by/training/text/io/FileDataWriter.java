package by.training.text.io;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;


/**
 * The type File data writer.
 * Created on 25.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class FileDataWriter {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");

    /**
     * Write string in file.
     *
     * @param fileName the file name
     * @param text     the text for file recording
     */
    public void writStringInFile(final String fileName, final String text) {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(fileName))) {
            writer.write(text);
            writer.flush();

        } catch (IOException exc) {
            LOG.log(Level.INFO, exc.getMessage());
        }

    }
}
