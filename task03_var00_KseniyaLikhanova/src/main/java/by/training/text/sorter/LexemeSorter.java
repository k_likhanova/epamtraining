package by.training.text.sorter;

import by.training.text.composite.Component;
import by.training.text.composite.Composite;
import by.training.text.composite.CompositeLeaf;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * The type Lexeme sorter.
 * Created on 27.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public final class LexemeSorter {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");

    /**
     * Private constructor, because all method is static.
     */
    private LexemeSorter() {
    }

    /**
     * Sort lexemes in text on decrease occurrences amount
     * of the concrete symbol,
     * and in case of equality - alphabetically.
     *
     * @param composite the composite
     * @param symbol    the symbol by which it is sorted
     * @return the sorted text
     */
    public static Component sortByConcreteSymbolAmount(
                        final Component composite, final char symbol) {
        Component text = new Composite(Component.ComponentType.TEXT);
        if (composite != null
                && composite.getType() == Component.ComponentType.TEXT
                && composite.hasChildren()) {
            List<Component> lexemes = new LinkedList<>();
            for (Component paragraph : composite.getChildren()) {
                for (Component sentence : paragraph.getChildren()) {
                    lexemes.addAll(sentence.getChildren());
                }
            }
            lexemes.sort(Comparator.comparingInt(lexeme ->
                                countSymbolAmount((Composite) lexeme, symbol))
                    .reversed()
                    .thenComparing(lexeme -> lexeme.toString().toLowerCase()));


            ((Composite) text)
                    .setChildren(List.of(new Composite(
                                           Component.ComponentType.PARAGRAPH)));
            Composite paragraph = (Composite) text.getChildren().get(0);
            paragraph.setChildren(List.of(new Composite(
                                           Component.ComponentType.SENTENCE)));
            Composite sentence = (Composite) text.getChildren().get(0);
            sentence.setChildren(lexemes);
        } else {
            LOG.log(Level.INFO, "This composite isn`t a text");
        }
        return text;
    }

    /**
     * Counts the amount of concrete symbol in composite.
     *
     * @param composite the composite
     * @param symbol    the symbol by which it is counted
     * @return amount of symbol
     */
    public static int countSymbolAmount(final Component composite,
                                        final Character symbol) {
        int result = 0;
        if (composite != null) {
            if (composite.hasChildren()) {
                for (Component component : composite.getChildren()) {
                    result += countSymbolAmount(component, symbol);
                }
            } else {
                CompositeLeaf leaf = (CompositeLeaf) composite;
                if (leaf.toString().equalsIgnoreCase(symbol.toString())) {
                    result = 1;
                }
            }
        } else {
            LOG.log(Level.INFO, "This composite isn`t a lexeme");
        }
        return result;
    }
}
