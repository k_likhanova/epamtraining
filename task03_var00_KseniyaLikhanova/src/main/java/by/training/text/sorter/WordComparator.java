package by.training.text.sorter;

import by.training.text.composite.Component;

import java.util.Comparator;

/**
 * The type word Comparator.
 * Created on 25.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class WordComparator implements Comparator<Component> {
    /**
     * Compares its two arguments for order.  Returns a negative integer,
     * zero, or a positive integer as the first argument is less than, equal
     * to, or greater than the second.
     *
     * @param firstLexeme the first object to be compared.
     * @param secondLexeme the second object to be compared.
     * @return a negative integer, zero, or a positive integer as the
     * first argument is less than, equal to, or greater than the
     * second.
     * @throws NullPointerException if an argument is null and this
     *                              comparator does not permit null arguments
     * @throws ClassCastException   if the arguments' types prevent them from
     *                              being compared by this comparator.
     */
    @Override
    public int compare(final Component firstLexeme,
                       final Component secondLexeme) {
        String firstWord = "";
        String secondWord = "";

        for (Component component : firstLexeme.getChildren()) {
            if (component.getType() != Component.ComponentType.PUNCTUATION) {
                firstWord = component.toString();
                break;
            }
        }

        for (Component component : secondLexeme.getChildren()) {
            if (component.getType() != Component.ComponentType.PUNCTUATION) {
                secondWord = component.toString();
                break;
            }
        }
        return firstWord.length() - secondWord.length();
    }
}
