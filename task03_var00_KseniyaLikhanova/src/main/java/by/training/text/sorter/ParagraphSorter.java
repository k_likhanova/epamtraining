package by.training.text.sorter;

import by.training.text.composite.Component;
import by.training.text.composite.Composite;
import by.training.text.handler.collector.ParserCollector;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;

/**
 * The type Paragraph sorter.
 * Created on 25.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public final class ParagraphSorter {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");

    /**
     * Private constructor, because all method is static.
     */
    private ParagraphSorter() {
    }

    /**
     * Sort paragraphs by sentences amount.
     *
     * @param composite the composite
     * @return the sorted text
     */
    public static Component sortBySentencesAmount(final Component composite) {
        Component text = new Composite(Component.ComponentType.TEXT);
        if (composite != null
                && composite.getType() == Component.ComponentType.TEXT
                && composite.hasChildren()) {
            text = new ParserCollector().parseComponents(composite.toString());
            text.getChildren().sort(Comparator.comparingInt(
                    paragraph -> paragraph.getChildren().size()));
        } else {
            LOG.log(Level.INFO, "This composite isn`t a text");
        }
        return text;
    }
}
