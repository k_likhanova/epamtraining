package by.training.text.sorter;

import by.training.text.composite.Component;
import by.training.text.composite.Composite;
import by.training.text.handler.collector.ParserCollector;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The type Word sorter.
 * Created on 25.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public final class WordSorter {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");

    /**
     * Private constructor, because all method is static.
     */
    private WordSorter() { }

    /**
     * Sort words in sentences by length.
     *
     * @param composite the composite
     * @return the sorted text
     */
    public static Component sortWordsByLength(final Component composite) {
        Component text = new Composite(Component.ComponentType.TEXT);
        if (composite != null
                && composite.getType() == Component.ComponentType.TEXT
                && composite.hasChildren()) {
            text = new ParserCollector().parseComponents(composite.toString());
            WordComparator comparator = new WordComparator();
            for (Component paragraph : text.getChildren()) {
                for (Component sentence : paragraph.getChildren()) {
                    sentence.getChildren().sort(comparator);
                }
            }
        } else {
            LOG.log(Level.INFO, "This composite isn`t a text");
        }
        return text;
    }
}
