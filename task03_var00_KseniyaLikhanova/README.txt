Assignment:
    To create the application parsing the text from the file and allowing
        to carry out three various operations with the text.
    To restore the parsed text in original form.
    To calculate values bit the expressions which are found in the text.
    To sort paragraphs by the number of sentence.
    In each sentence to sort words by length.
    To sort lexemes in the text on decrease of number of occurrences
    the set symbol, and in case of equality – alphabetically.

For this project applied such patterns as: Composite, Chain of Responsibility, Interpreter.
The code corresponds CheckStyle, SonarLint, FindBugs.
To launch app required java 8.

Data with text for parsing in file data/Text.txt.
Data with text after parsing in file data/TextAfterCollector.txt.
Log4j configuration in file src/main/resources/log4j2.xml
TestNG configuration in file src/main/resources/testng.xml
The necessary libs describes in pom.xml.

Classes are covered with tests.
