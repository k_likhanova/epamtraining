package by.training.xmlparsing.builder;

import by.training.xmlparsing.xmlentity.VisualParameters;
import by.training.xmlparsing.xmlentity.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.time.LocalDate;

/**
 * The type DOM builder.
 * Created on 4.12.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class DOMBuilder extends Builder {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * The constant field specifies the symbol "-".
     */
    private static final String DASH = "-";
    /**
     * The constant field specifies the symbol "_".
     */
    private static final String UNDERLINE = "_";
    /**
     * The field specifies the type of documentBuilder.
     */
    private DocumentBuilder docBuilder;

    /**
     * Instantiates a new Dom builder.
     */
    DOMBuilder() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            LOG.log(Level.ERROR, e.getMessage());
        }

    }

    /**
     * Build flowers.
     *
     * @param fileName the file name
     */
    @Override
    public void buildFlowers(final String fileName) {
        Document doc;
        try {
            doc = docBuilder.parse(fileName);
            Element root = doc.getDocumentElement();
            NodeList flowersList = root.getElementsByTagName("flower");
            for (int i = 0; i < flowersList.getLength(); i++) {
                flowers.add(buildFlower((Element) flowersList.item(i)));
            }
        } catch (NoSuchFileException e) {
            LOG.log(Level.FATAL,
                    "File with name \"" + fileName + "\" not found.",
                    e);
        } catch (IOException e) {
            LOG.log(Level.FATAL, "Unable to open file.", e);
        } catch (SAXException e) {
            LOG.log(Level.FATAL, "Parsing failed.", e);
        }
    }

    /**
     * Create flower.
     *
     * @param flowerElement the flower element
     * @return flower
     */
    private Flower buildFlower(final Element flowerElement) {
        Flower flower = new Flower();
        flower.setId(flowerElement.getAttribute("id"));
        flower.setName(flowerElement.getAttribute("name"));

        flower.setSoil(Soil.valueOf(getElementTextContent(flowerElement,
                FlowersStructure.SOIL.getValue())
                .toUpperCase().replace(DASH, UNDERLINE)));

        flower.setOrigin(getElementTextContent(flowerElement, "origin"));

        VisualParameters visual = new VisualParameters();
        Element visualElement = (Element) flowerElement
                            .getElementsByTagName("visual-parameters").item(0);

        visual.setStemColor(getElementTextContent(visualElement,
                                        "stem-color"));
        visual.setPetalColor(getElementTextContent(visualElement,
                                        "petal-color"));

        visual.setAvgSize(new VisualParameters.AvgSize(Double.parseDouble(
                            getElementTextContent(visualElement, "avg-size"))));
        flower.setVisualParameters(visual);

        GrowingTips growing = new GrowingTips();
        Element growingElement = (Element) flowerElement
                            .getElementsByTagName("growing-tips").item(0);

        growing.setTemperature(new GrowingTips.Temperature(Integer.parseInt(
                        getElementTextContent(growingElement, "temperature"))));

        growing.setLighting(Boolean.parseBoolean(
                        getElementTextContent(growingElement, "lighting")));

        growing.setWatering(new GrowingTips.Watering(Integer.parseInt(
                        getElementTextContent(growingElement, "watering"))));
        flower.setGrowingTips(growing);

        flower.setMultiplying(Multiplying.valueOf(getElementTextContent(
                flowerElement, FlowersStructure.MULTIPLYING.getValue())
                .toUpperCase()));

        flower.setLandingDate(LocalDate.parse(
                        getElementTextContent(flowerElement, "landing-date")));
        return flower;
    }

    /**
     * Takes text contents of the tag.
     * @param element the tag
     * @param elementName the name of tag
     * @return text
     */
    private static String getElementTextContent(final Element element,
                                                final String elementName) {
        return element.getElementsByTagName(elementName)
                      .item(0).getTextContent();
    }
}
