package by.training.xmlparsing.builder;

import by.training.xmlparsing.xmlentity.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;

/**
 * The type StAX builder.
 * Created on 4.12.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class StAXBuilder extends Builder {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * The constant field specifies the symbol "-".
     */
    private static final String DASH = "-";
    /**
     * The constant field specifies the symbol "_".
     */
    private static final String UNDERLINE = "_";
    /**
     * The field specifies the type of XMLInputFactory.
     */
    private XMLInputFactory inputFactory;

    /**
     * Instantiates a new StAX builder.
     */
    StAXBuilder() {
        inputFactory = XMLInputFactory.newInstance();
    }

    /**
     * Build flowers.
     *
     * @param fileName the file name
     */
    @Override
    public void buildFlowers(final String fileName) {
        XMLStreamReader reader;
        try (FileInputStream inputStream = new FileInputStream(
                                                    new File(fileName))) {
            reader = inputFactory.createXMLStreamReader(inputStream);
            while (reader.hasNext()) {
                if (reader.next() == XMLStreamConstants.START_ELEMENT
                        && FlowersStructure.valueOf(
                                  reader.getLocalName()
                                        .toUpperCase().replace(DASH, UNDERLINE))
                                            == FlowersStructure.FLOWER) {
                    flowers.add(buildFlower(reader));
                }
            }
        } catch (XMLStreamException e) {
            LOG.log(Level.FATAL, "Parsing failed.", e);
        } catch (FileNotFoundException e) {
            LOG.log(Level.FATAL,
                    "File with name \"" + fileName + "\" not found.",
                    e);
        } catch (IOException e) {
            LOG.log(Level.FATAL, "Unable to open file.", e);
        }
    }

    /**
     * Build flower.
     *
     * @param reader the XMLStreamReader
     * @return the flower
     * @throws XMLStreamException if flower has unknown element in tag Flower
     */
    private Flower buildFlower(final XMLStreamReader reader)
                                        throws XMLStreamException {
        Flower flower = new Flower();
        FlowersStructure currentElement;

        flower.setId(reader.getAttributeValue(
                    null, FlowersStructure.ID.getValue()));
        flower.setName(reader.getAttributeValue(
                    null, FlowersStructure.NAME.getValue()));
        int type;
        while (reader.hasNext()) {
            type = reader.next();
            if (type == XMLStreamConstants.START_ELEMENT) {
                currentElement = FlowersStructure
                                    .valueOf(reader.getLocalName()
                                                   .toUpperCase()
                                                   .replace(DASH, UNDERLINE));
                switch (currentElement) {
                    case SOIL:
                        flower.setSoil(Soil.valueOf(getXMLText(reader)
                                            .toUpperCase()
                                            .replace(DASH, UNDERLINE)));
                        break;
                    case ORIGIN:
                        flower.setOrigin(getXMLText(reader));
                        break;
                    case VISUAL_PARAMETERS:
                        flower.setVisualParameters(
                                    getXMLVisualParameters(reader));

                        break;
                    case GROWING_TIPS:
                        flower.setGrowingTips(getXMLGrowingTips(reader));
                        break;
                    case LANDING_DATE:
                        flower.setLandingDate(LocalDate
                                                .parse(getXMLText(reader)));
                        break;
                    case MULTIPLYING:
                        flower.setMultiplying(Multiplying
                                                .valueOf(getXMLText(reader)
                                                            .toUpperCase()));
                        break;
                    default:
                        throw new EnumConstantNotPresentException(
                                currentElement.getDeclaringClass(),
                                currentElement.name());
                }
            } else if (type == XMLStreamConstants.END_ELEMENT
                    && FlowersStructure.valueOf(reader.getLocalName()
                                                      .toUpperCase()
                                                      .replace(DASH, UNDERLINE))
                                           == FlowersStructure.FLOWER) {
                return flower;
            }
        }
        throw new XMLStreamException("Unknown element in tag Flower");
    }

    /**
     * Gets visual parameters of flower.
     *
     * @param reader the XMLStreamReader
     * @return the visual parameters
     * @throws XMLStreamException the XMLStream exception
     */
    private VisualParameters getXMLVisualParameters(
                final XMLStreamReader reader) throws XMLStreamException {
        VisualParameters visual = new VisualParameters();
        FlowersStructure currentElement;
        int type;

        while (reader.hasNext()) {
            type = reader.next();
            if (type == XMLStreamConstants.START_ELEMENT) {
                currentElement = FlowersStructure.valueOf(
                                        reader.getLocalName()
                                              .toUpperCase()
                                              .replace(DASH, UNDERLINE));
                switch (currentElement) {
                    case STEM_COLOR:
                        visual.setStemColor(getXMLText(reader));
                        break;
                    case PETAL_COLOR:
                        visual.setPetalColor(getXMLText(reader));
                        break;
                    case AVG_SIZE:
                        visual.setAvgSize(new VisualParameters.AvgSize(
                                    Double.parseDouble(getXMLText(reader))));
                        break;
                    default:
                        throw new EnumConstantNotPresentException(
                                currentElement.getDeclaringClass(),
                                currentElement.name());
                }
            } else if (type == XMLStreamConstants.END_ELEMENT
                    && FlowersStructure.valueOf(reader.getLocalName()
                                                      .toUpperCase()
                                                      .replace(DASH, UNDERLINE))
                            == FlowersStructure.VISUAL_PARAMETERS) {
                return visual;
            }
        }
        throw new XMLStreamException("Unknown element in Visual-parameters");
}

    /**
     * Gets growing tips of flower.
     *
     * @param reader the XMLStreamReader
     * @return the growing-tips
     * @throws XMLStreamException if flower has unknown element in Growing-tips
     */
    private GrowingTips getXMLGrowingTips(
            final XMLStreamReader reader) throws XMLStreamException {
        GrowingTips growing = new GrowingTips();
        FlowersStructure currentElement;
        int type;

        while (reader.hasNext()) {
            type = reader.next();
            if (type == XMLStreamConstants.START_ELEMENT) {
                currentElement = FlowersStructure
                                        .valueOf(reader.getLocalName()
                                                       .toUpperCase()
                                                       .replace(DASH, UNDERLINE));
                switch (currentElement) {
                    case TEMPERATURE:
                        growing.setTemperature(new GrowingTips.Temperature(
                                        Integer.parseInt(getXMLText(reader))));
                        break;
                    case LIGHTING:
                        growing.setLighting(
                                      Boolean.parseBoolean(getXMLText(reader)));
                        break;
                    case WATERING:
                        growing.setWatering(new GrowingTips.Watering(
                                      Integer.parseInt(getXMLText(reader))));
                        break;
                    default:
                        throw new EnumConstantNotPresentException(
                                currentElement.getDeclaringClass(),
                                currentElement.name());
                }
            } else if (type == XMLStreamConstants.END_ELEMENT
                    && FlowersStructure.valueOf(reader.getLocalName()
                        .toUpperCase().replace(DASH, UNDERLINE))
                                        == FlowersStructure.GROWING_TIPS) {
                return growing;
            }
        }
        throw new XMLStreamException("Unknown element in Growing-tips");
    }

    /**
     * Gets xml text.
     *
     * @param reader the XMLStreamReader
     * @return the text
     * @throws XMLStreamException the XMLStream exception
     */
    private String getXMLText(final XMLStreamReader reader)
                                        throws XMLStreamException {
        String text = "";
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}
