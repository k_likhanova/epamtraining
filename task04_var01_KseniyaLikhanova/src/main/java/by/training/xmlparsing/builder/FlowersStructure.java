package by.training.xmlparsing.builder;

/**
 * The enum Flowers structure.
 * Created on 4.12.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public enum FlowersStructure {
    /**
     * The root element.
     */
    FLOWERS("flowers"),
    /**
     * Flower.
     */
    FLOWER("flower"),
    /**
     * Flower id.
     */
    ID("id"),
    /**
     * Flower name.
     */
    NAME("name"),
    /**
     * Flower soil.
     */
    SOIL("soil"),
    /**
     * Flower origin.
     */
    ORIGIN("origin"),
    /**
     * Flower stem color.
     */
    STEM_COLOR("stem-color"),
    /**
     * Flower petal color.
     */
    PETAL_COLOR("petal-color"),
    /**
     * Flower avg size.
     */
    AVG_SIZE("avg-size"),
    /**
     * Flower temperature.
     */
    TEMPERATURE("temperature"),
    /**
     * Flower lighting.
     */
    LIGHTING("lighting"),
    /**
     * Flower watering.
     */
    WATERING("watering"),
    /**
     * Flower multiplying.
     */
    MULTIPLYING("multiplying"),
    /**
     * Flower landing date.
     */
    LANDING_DATE("landing-date"),
    /**
     * Flower visual parameters.
     */
    VISUAL_PARAMETERS("visual-parameters"),
    /**
     * Flower growing tips.
     */
    GROWING_TIPS("growing-tips");

    /**
     * Value of enum element.
     */
    private String value;

    /**
     * Instance a new FlowerStructure.
     *
     * @param newValue the value of enum element
     */
    FlowersStructure(final String newValue) {
        this.value = newValue;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Determines if there is contains tag.
     *
     * @param tag the tag
     * @return the boolean
     */
    public static boolean isContainsTag(final String tag) {
        boolean isContains = false;
        for (FlowersStructure element : values()) {
            if (tag.equals(element.name())) {
                isContains = true;
            }
        }
        return isContains;
    }
}
