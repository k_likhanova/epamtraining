package by.training.xmlparsing.builder;

import by.training.xmlparsing.xmlentity.Flowers;

/**
 * The type abstract builder.
 * Created on 30.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public abstract class Builder {
    /**
     * The Flowers.
     */
    Flowers flowers;

    /**
     * Instantiates a new Builder.
     */
    Builder() {
        this.flowers = new Flowers();
    }

    /**
     * Gets flowers.
     *
     * @return the flowers
     */
    public Flowers getFlowers() {
        return flowers;
    }

    /**
     * Build flowers.
     *
     * @param fileName the file name
     */
    public abstract void buildFlowers(String fileName);
}
