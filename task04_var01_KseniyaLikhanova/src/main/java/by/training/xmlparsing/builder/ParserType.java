package by.training.xmlparsing.builder;

/**
 * The enum Parser type.
 * Created on 30.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public enum ParserType {
    /**
     * DOM parser type.
     */
    DOM,
    /**
     * SAX parser type.
     */
    SAX,
    /**
     * StAX parser type.
     */
    STAX;

    /**
     * Determines if there is contains builder boolean.
     *
     * @param type the type of parser
     * @return the boolean
     */
    public static boolean isContainsParser(final String type) {
        boolean isContains = false;
        for (ParserType element : values()) {
            if (type.equals(element.name())) {
                isContains = true;
            }
        }
        return isContains;
    }
}
