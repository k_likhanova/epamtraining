package by.training.xmlparsing.builder;

import org.apache.logging.log4j.Level;
import org.xml.sax.XMLReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * The type SAX builder.
 * Created on 4.12.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class SAXBuilder extends Builder {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * The field specifies the type of SAXHandler.
     */
    private SAXHandler handler;
    /**
     * The field specifies the type of XMLReader.
     */
    private XMLReader reader;

    /**
     * Instantiates a new SAX builder.
     */
    SAXBuilder() {
        handler = new SAXHandler();
        try {
            reader = SAXParserFactory.newInstance()
                        .newSAXParser().getXMLReader();
            reader.setContentHandler(handler);
        } catch (ParserConfigurationException | SAXException e) {
            LOG.log(Level.ERROR, e.getMessage());
        }
    }

    /**
     * Build flowers.
     *
     * @param fileName the file name
     */
    @Override
    public void buildFlowers(final String fileName) {
        try {
            reader.parse(fileName);
        } catch (SAXException | IOException e) {
            LOG.log(Level.ERROR, e.getMessage());
        }
        flowers = handler.getFlowers();
    }
}
