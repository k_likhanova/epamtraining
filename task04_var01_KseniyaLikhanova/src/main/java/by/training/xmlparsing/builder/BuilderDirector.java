package by.training.xmlparsing.builder;

import java.util.EnumMap;

/**
 * The type director of builder.
 * Created on 30.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public final class BuilderDirector {
    /**
     * Field instance specifies the single object
     *              of BuilderDirector.
     */
    private static BuilderDirector instance = new BuilderDirector();
    /**
     * The field connected type of parser and object of parser.
     */
    private EnumMap<ParserType, Builder> builders;

    /**
     * Instantiates a new Builder director.
     */
    private BuilderDirector() {
        builders = new EnumMap<>(ParserType.class);
        builders.put(ParserType.SAX, new SAXBuilder());
        builders.put(ParserType.STAX, new StAXBuilder());
        builders.put(ParserType.DOM, new DOMBuilder());
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static BuilderDirector getInstance() {
        return instance;
    }
    /**
     * Take object of builder by name.
     *
     * @param name of builder
     * @return the builder
     */
    public Builder takeBuilder(final String name) {
        return builders.get(ParserType.valueOf(name));
    }
}
