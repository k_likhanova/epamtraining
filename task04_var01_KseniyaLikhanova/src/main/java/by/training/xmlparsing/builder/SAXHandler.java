package by.training.xmlparsing.builder;

import by.training.xmlparsing.xmlentity.*;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.time.LocalDate;
import java.util.EnumSet;

/**
 * The type SAX handler.
 * Created on 4.12.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class SAXHandler extends DefaultHandler {
    /**
     * The constant field specifies the symbol "-".
     */
    private static final String DASH = "-";
    /**
     * The constant field specifies the symbol "_".
     */
    private static final String UNDERLINE = "_";
    /**
     * The Flowers.
     */
    private Flowers flowers;
    /**
     * The Flower.
     */
    private Flower currentFlower;
    /**
     * The field specifies the type of enum of FlowersStructure.
     */
    private FlowersStructure currentElement;
    /**
     * EnumSet of flower structure parameters.
     */
    private EnumSet<FlowersStructure> withText;

    /**
     * Instantiates a new SAX handler.
     */
    SAXHandler() {
        flowers = new Flowers();
        withText = EnumSet.range(FlowersStructure.SOIL,
                                 FlowersStructure.LANDING_DATE);
    }

    /**
     * Gets flowers.
     *
     * @return the flowers
     */
    public Flowers getFlowers() {
        return flowers;
    }

    @Override
    public void startElement(final String uri, final String localName,
                             final String qName, final Attributes attributes) {
        if ("flower".equals(qName)) {
            currentFlower = new Flower();
            if (attributes.getLength() == 2) {
                currentFlower.setId(
                        attributes.getValue(FlowersStructure.ID.getValue()));
                currentFlower.setName(
                        attributes.getValue(FlowersStructure.NAME.getValue()));
            }
        } else {
            String elementName = qName.toUpperCase().replace(DASH, UNDERLINE);
            if (FlowersStructure.isContainsTag(elementName)) {
                FlowersStructure temp = FlowersStructure.valueOf(elementName);
                if (withText.contains(temp)) {
                    currentElement = temp;
                }
            }
        }
    }

    @Override
    public void endElement(final String uri,
                           final String localName,
                           final String qName) {
        if ("flower".equals(qName)) {
            flowers.add(currentFlower);
        }
    }

    @Override
    public void characters(final char[] ch, final int start, final int length) {
        String text = new String(ch, start, length).trim();
        if (currentElement != null) {
            switch (currentElement) {
                case SOIL:
                    currentFlower.setSoil(
                                Soil.valueOf(text.toUpperCase()
                                                 .replace(DASH, UNDERLINE)));
                    break;
                case ORIGIN:
                    currentFlower.setOrigin(text);
                    break;
                case STEM_COLOR:
                    currentFlower.getVisualParameters().setStemColor(text);
                    break;
                case PETAL_COLOR:
                    currentFlower.getVisualParameters().setPetalColor(text);
                    break;
                case AVG_SIZE:
                    currentFlower.getVisualParameters().setAvgSize(
                            new VisualParameters.AvgSize(Double.parseDouble(text)));
                    break;
                case TEMPERATURE:
                    currentFlower.getGrowingTips().setTemperature(
                            new GrowingTips.Temperature(Integer.parseInt(text)));
                    break;
                case LIGHTING:
                    currentFlower.getGrowingTips()
                                    .setLighting(Boolean.parseBoolean(text));
                    break;
                case WATERING:
                    currentFlower.getGrowingTips().setWatering(
                            new GrowingTips.Watering(Integer.parseInt(text)));
                    break;
                case MULTIPLYING:
                    currentFlower.setMultiplying(Multiplying
                                                .valueOf(text.toUpperCase()));
                    break;
                case LANDING_DATE:
                    currentFlower.setLandingDate(LocalDate.parse(text));
                    break;
                default:
                    throw new EnumConstantNotPresentException(
                            currentElement.getDeclaringClass(),
                            currentElement.name()
                    );
            }
        }
        currentElement = null;
    }

}
