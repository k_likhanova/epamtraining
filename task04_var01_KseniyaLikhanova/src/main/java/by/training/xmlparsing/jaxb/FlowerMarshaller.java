package by.training.xmlparsing.jaxb;

import by.training.xmlparsing.xmlentity.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.time.Month;

/**
 * The type Flower marshaller.
 * Created on 30.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class FlowerMarshaller {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");

    /**
     * Create xml.
     */
    public void createXml() {
        try {
            JAXBContext context = JAXBContext.newInstance(Flowers.class);
            Marshaller marshaller = context.createMarshaller();
            Flowers flowers = new Flowers();

            String greenColor = "green";
            Flower aloe = new Flower("ID-1", "Aloe", Soil.PODZOLIC,
                    "Madagascar",
                    new VisualParameters(greenColor, greenColor, 80.0),
                    new GrowingTips(29, true, 200),
                    Multiplying.LEAVERS, LocalDate.of(2016, Month.FEBRUARY, 1));
            flowers.add(aloe);
            Flower tulp = new Flower("ID-2", "Tulp", Soil.PODZOLIC,
                    "Netherlands",
                    new VisualParameters(greenColor, "yellow", 40.0),
                    new GrowingTips(22, true, 5000),
                    Multiplying.CUTTINGS, LocalDate.of(2016, Month.APRIL, 11));
            flowers.add(tulp);

            marshaller.marshal(flowers,
                    new FileOutputStream("data/flowersAfterMarshaller.xml"));
        } catch (FileNotFoundException | JAXBException e) {
            LOG.log(Level.ERROR, e.getMessage());
        }
    }
}
