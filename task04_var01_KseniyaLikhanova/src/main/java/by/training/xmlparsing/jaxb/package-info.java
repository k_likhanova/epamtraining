/**
 * Provides the classes are jaxb marshaller and unmarshaller.
 * Pattern Builder is used.
 */
package by.training.xmlparsing.jaxb;
