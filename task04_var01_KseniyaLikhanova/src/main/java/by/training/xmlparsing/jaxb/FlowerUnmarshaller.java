package by.training.xmlparsing.jaxb;

import by.training.xmlparsing.xmlentity.Flowers;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.FileReader;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * The type Flower unmarshaller.
 * Created on 30.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class FlowerUnmarshaller {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");

    /**
     * Creates flowers from xml.
     *
     * @return the flowers
     */
    public Flowers createFlowersFromXml() {
        Flowers flowers = new Flowers();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Flowers.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            FileReader reader = new FileReader("data/flowers.xml");
            flowers = (Flowers) unmarshaller.unmarshal(reader);
        } catch (JAXBException | FileNotFoundException e) {
            LOG.log(Level.ERROR, e.getMessage());
        }
        return flowers;
    }
}
