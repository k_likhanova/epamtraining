//package by.training.xmlparsing.controller;
//
//import by.tc.parsing.controller.util.ListOutputHelper;
//import by.tc.parsing.controller.util.Paginator;
//import by.tc.parsing.controller.validator.PageParamValidator;
//import by.tc.parsing.dao.parse.CommandName;
//import by.tc.parsing.entity.Flower;
//import by.tc.parsing.service.FlowerService;
//import by.tc.parsing.service.ServiceFactory;
//import by.tc.parsing.service.exception.ServiceException;
//import by.training.xmlparsing.builder.ParserType;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.List;
//
///**
// * Created by cplus on 19.11.2017.
// */
//public class Controller extends HttpServlet {
//    /**
//     * The constant LOG.
//     */
//    private static final Logger LOG = LogManager.getLogger("logger");
//    private static final long serialVersionUID = -1058916453755543319L;
//
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        ServiceFactory instance = ServiceFactory.getInstance();
//        FlowerService flowerService = instance.getFlowerService();
//
//        String commandParam = request.getParameter("command");
//
//        if (commandParam != null && ParserType.isContainsParser(commandParam)) {
//            try {
//                List<Flower> allFlowers = flowerService.parse(commandParam);
//                String pageParam = request.getParameter("page");
//                if (PageParamValidator.isValidPageParam(pageParam,ListOutputHelper.getNumberPages(allFlowers.size()))) {
//                    int currentPage = Integer.parseInt(pageParam);
//
//                    Paginator paginator = new Paginator(allFlowers.size(), currentPage);
//
//                    List<Flower> outPutFlowers = allFlowers.subList(ListOutputHelper.indexFirstElementOnPage(currentPage),
//                            ListOutputHelper.getIndexLastElementOnPage(paginator.getContentSize(), currentPage));
//
//                    request.setAttribute("currentPage", currentPage);
//                    request.setAttribute("nextPage", paginator.getNextPage());
//                    request.setAttribute("prevPage", paginator.getPreviousPage());
//                    request.setAttribute("lastPage", paginator.getLastPage());
//                    request.setAttribute("firstPage", paginator.getFirstPage());
//                    request.setAttribute("flowerList", outPutFlowers);
//                    request.setAttribute("command", commandParam);
//                    request.getRequestDispatcher("WEB-INF/flowersInfo.jsp").forward(request, response);
//                } else {
//                    request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
//                }
//            } catch (ServiceException e) {
//                LOG.error(e);
//            }
//        } else {
//            request.getRequestDispatcher("WEB-INF/error.jsp").forward(request, response);
//        }
//    }
//
//}
