package by.training.xmlparsing.builder;

import by.training.xmlparsing.xmlentity.*;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.Month;

public class BuilderTest {
    private Flowers expected;
    private final static String GREEN = "green";

    @BeforeClass
    public void setUp() {
        expected = new Flowers();
        expected.add(new Flower("ID-1", "Rose", Soil.GROUND,
                "Italy",
                new VisualParameters(GREEN, "red", 50.0),
                new GrowingTips(27, true, 5000),
                Multiplying.CUTTINGS, LocalDate.of(2016, Month.JANUARY, 25)));
        expected.add(new Flower("ID-2", "Camomile", Soil.GROUND,
                "Russia",
                new VisualParameters(GREEN, "white", 40.0),
                new GrowingTips(20, true, 1000),
                Multiplying.SEEDS, LocalDate.of(2018, Month.MAY, 14)));
        expected.add(new Flower("ID-3", "Tulp", Soil.PODZOLIC,
                "Netherlands",
                new VisualParameters(GREEN, "yellow", 40.0),
                new GrowingTips(22, true, 5000),
                Multiplying.CUTTINGS, LocalDate.of(2016, Month.APRIL, 11)));
        expected.add(new Flower("ID-4", "Aloe", Soil.PODZOLIC,
                "Madagascar",
                new VisualParameters(GREEN, GREEN, 80.0),
                new GrowingTips(29, true, 200),
                Multiplying.LEAVERS, LocalDate.of(2016, Month.FEBRUARY, 1)));
    }

    @DataProvider(name = "builder")
    public static Object[][] builder() {
        return new Object[][]{
                {"SAX"}, {"STAX"}, {"DOM"}
        };
    }

    @Test(dataProvider = "builder")
    public void testBuildFlowers(final String parserType) {
        Builder builder = BuilderDirector.getInstance().takeBuilder(parserType);
        builder.buildFlowers("data/flowers.xml");
        Flowers flowers = builder.getFlowers();
        Assert.assertEquals(flowers.getFlower().subList(0, 4).toArray(),
                            expected.getFlower().toArray());
    }
}
