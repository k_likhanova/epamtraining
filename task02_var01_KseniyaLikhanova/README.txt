Assignment:
    The ships come into the port for unloading
        and/or loading of containers and are moored to moorings.
    Each mooring can have only one ship.
    Containers are loaded from the ship on a warehouse of port
        and/or from a warehouse on the ship.
    Each ship has to be surely served.

For this project applied such patterns as: Singleton.
The code corresponds CheckStyle, SonarLint, FindBugs.
To launch app required java 8.

Data for creating objects in file data/Ships.txt.
Log4j configuration in file src/main/resources/log4j2.xml
The necessary libs describes in pom.xml.

