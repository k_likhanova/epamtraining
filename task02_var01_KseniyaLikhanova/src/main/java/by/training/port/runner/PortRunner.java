package by.training.port.runner;

import by.training.port.entity.Ship;
import by.training.port.manager.ShipManager;

import by.training.port.thread.ShipThread;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Class is point of entry in program.
 * Created on 13.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class PortRunner {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * Field FILE_PATH specifies the file name.
     */
    private static final String FILE_PATH = "data/Ships.txt";

    /**
     * Point of entry in program.
     *
     * @param args the arguments
     * @throws IOException            the exception
     */
    public static void main(final String[] args)
                                        throws IOException {
        ShipManager shipManager = new ShipManager();
        List<Ship> ships = shipManager.fillShips(FILE_PATH);
        List<ShipThread> shipThreads = new LinkedList<>();
        for (Ship ship : ships) {
            shipThreads.add(new ShipThread(ship));
        }
        final int shipsAmountInPort = 10;
        ExecutorService executor =
                Executors.newFixedThreadPool(shipsAmountInPort);
        List<Future<Long>> futureList = new LinkedList<>();
        for (ShipThread shipThread : shipThreads) {
            futureList.add(executor.submit(shipThread));
        }
        for (Future<Long> fut : futureList) {
            try {
                LOG.log(Level.INFO, fut.get());
            } catch (InterruptedException | ExecutionException exc) {
                LOG.log(Level.ERROR, exc.getMessage());
                Thread.currentThread().interrupt();
            }
        }
        executor.shutdown();
    }
}
