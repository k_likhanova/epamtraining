package by.training.port.manager;

import by.training.port.creator.ShipCreator;
import by.training.port.customexception.IllegalObjectException;
import by.training.port.entity.Ship;
import by.training.port.parser.Parser;
import by.training.port.reader.FileDataReader;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * The type Ship manager.
 * Created on 13.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class ShipManager {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");
    /**
     * Field reader specifies the object of FileDataReader.
     */
    private FileDataReader reader;
    /**
     * Field parser specifies the object of Parser.
     */
    private Parser parser;
    /**
     * Field creator specifies the object of Creator.
     */
    private ShipCreator creator;

    /**
     * Instantiates a new Ship manager.
     */
    public ShipManager() {
        reader = new FileDataReader();
        parser = new Parser();
        creator = new ShipCreator();
    }

    /**
     * Fill list of ships.
     *
     * @param fileName the file name
     * @return the list of ships
     * @throws IOException the io exception
     */
    public List<Ship> fillShips(final String fileName)
                                                throws IOException {
        List<Ship> ships = new LinkedList<>();
        List<String[]> fileStrings = parser.parseFileStrings(
                                        reader.readFromFile(fileName));
        for (String[] fileString : fileStrings) {
            try {
                ships.add(creator.create(parser.parseStringToLong(fileString)));
            } catch (IllegalObjectException exc) {
                LOG.log(Level.INFO, exc.getMessage());
            }
        }
        return ships;
    }

}
