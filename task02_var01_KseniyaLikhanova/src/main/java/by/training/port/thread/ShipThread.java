package by.training.port.thread;

import by.training.port.action.WorkWithContainersInPort;
import by.training.port.entity.Port;
import by.training.port.entity.Ship;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * The type Ship thread.
 * Created on 14.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class ShipThread extends Ship implements Callable<Long> {

    /**
     * The constant specifies the logger.
     */
    private static final Logger LOG = LogManager.getLogger("logger");

    /**
     * Instantiates a new Ship.
     *
     * @param newId                        the new id
     * @param newCapacity                  the new capacity
     * @param newCurrentCapacity           the new current capacity
     * @param newLoadingContainersAmount   the new loading containers amount
     * @param newUnloadingContainersAmount the new unloading containers amount
     */
    public ShipThread(final long newId,
                      final long newCapacity,
                      final long newCurrentCapacity,
                      final long newLoadingContainersAmount,
                      final long newUnloadingContainersAmount) {
        super(newId, newCapacity, newCurrentCapacity,
              newLoadingContainersAmount, newUnloadingContainersAmount);
    }

    /**
     * Instantiates a new Ship.
     *
     * @param ship the ship
     */
    public ShipThread(final Ship ship) {
        super(ship.getId(), ship.getCapacity(),
              ship.getCurrentCapacity(),
              ship.getLoadingContainersAmount(),
              ship.getUnloadingContainersAmount());
    }

    /**
     * Starts a thread.
     * If there is available mooring in port then carry out task.
     * If there isn't then wait.
     * 5 attempts are given on performance of a task.
     *
     * @return id of Ship
     */
    @Override
    public Long call() {
        WorkWithContainersInPort port = new WorkWithContainersInPort();
        try {
            Port.getSemaphore().acquire();
            LOG.log(Level.INFO, "Ship #" + super.getId()
                                    + " arrived to the mooring.");
            boolean isNotEnded = true;
            final int attempts = 5;
            for (int i = 0; i < attempts && isNotEnded; i++) {
                if (super.getUnloadingContainersAmount() > 0) {
                    unloadShip(port);
                }
                TimeUnit.MILLISECONDS.sleep(30);
                if (super.getLoadingContainersAmount() > 0) {
                    loadShip(port);
                }
                if (super.getLoadingContainersAmount() > 0
                        || super.getUnloadingContainersAmount() > 0) {
                    TimeUnit.SECONDS.sleep(1);
                } else {
                    isNotEnded = false;
                }
            }
        } catch (InterruptedException exc) {
            LOG.log(Level.ERROR, exc.getMessage());
            Thread.currentThread().interrupt();
        } catch (Exception exc) {
            LOG.log(Level.ERROR, exc.getMessage());
        } finally {
            Port.getSemaphore().release();
            LOG.log(Level.INFO, "Ship #" + super.getId() + " has been served.");
        }
        return super.getId();
    }

    /**
     * Loads containers on the Ship.
     *
     * @param port the object of work with containers in port
     */
    private void loadShip(final WorkWithContainersInPort port) {
        try {
            WorkWithContainersInPort.getSemaphore().acquire();
            if (!port.loadShip(this)) {
                LOG.log(Level.INFO,
                        "There isn't enough capacity for loading of "
                                + "the ship #" + super.getId());

            }
        } catch (InterruptedException exc) {
            LOG.log(Level.ERROR, exc.getMessage());
            Thread.currentThread().interrupt();
        } finally {
            WorkWithContainersInPort.getSemaphore().release();
        }
    }

    /**
     * Unloads containers on the Ship.
     *
     * @param port the object of work with containers in port
     */
    private void unloadShip(final WorkWithContainersInPort port) {
        try {
            WorkWithContainersInPort.getSemaphore().acquire();
            if (!port.unloadShip(this)) {
                LOG.log(Level.INFO,
                        "There isn't enough capacity for unloading"
                                + " of the ship #" + super.getId());
            }
        } catch (InterruptedException exc) {
            LOG.log(Level.ERROR, exc.getMessage());
            Thread.currentThread().interrupt();
        } finally {
            WorkWithContainersInPort.getSemaphore().release();
        }
    }
}
