package by.training.port.parser;

import by.training.port.validator.FileValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

/**
 * The type Parser.
 * Created on 13.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class Parser {
    /**
     * The constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger("logger");

    /**
     * Parse file strings list.
     * Lines not corresponding validLine are passed.
     *
     * @param fileStrings the file strings
     * @return the valid strings list
     */
    public List<String[]> parseFileStrings(final List<String> fileStrings) {
        List<String[]> validFileStrings = new LinkedList<>();
        for (String fileString : fileStrings) {
            if (FileValidator.validateLine(fileString)) {
                validFileStrings.add(fileString.trim().split(" "));
            } else {
                LOG.log(Level.INFO, fileString + " is not valid.");
            }
        }
        return validFileStrings;
    }

    /**
     * Parse file string to double.
     *
     * @param string the string
     * @return the list
     */
    public List<Long> parseStringToLong(final String[] string) {
        List<Long>  numbers = new LinkedList<>();
        for (int i = 0; i < string.length; i++) {
            numbers.add(Long.parseLong(string[i]));
        }
        return numbers;
    }
}
