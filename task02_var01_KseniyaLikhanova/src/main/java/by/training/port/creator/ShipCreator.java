package by.training.port.creator;

import by.training.port.customexception.IllegalObjectException;
import by.training.port.entity.Ship;
import by.training.port.validator.ShipValidator;

import java.util.List;

/**
 * The type Ship creator.
 * Created on 13.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class ShipCreator {

    /**
     * The constant specifies number of element.
     */
    private static final int ID = 0;
    /**
     * The constant specifies number of element.
     */
    private static final int CAPACITY = 1;
    /**
     * The constant specifies number of element.
     */
    private static final int CURRENT_CAPACITY = 2;
    /**
     * The constant specifies number of element.
     */
    private static final int LOADING_CONTAINER_AMOUNT = 3;
    /**
     * The constant specifies number of element.
     */
    private static final int UNLOADING_CONTAINER_AMOUNT = 4;

    /**
     * Create ship.
     *
     * @param data the data of the ship
     * @return the ship
     * @throws IllegalObjectException the illegal object exception
     */
    public Ship create(final List<Long> data) throws IllegalObjectException {
        if (ShipValidator.isCorrectData(data)) {
            return new Ship(data.get(ID),
                    data.get(CAPACITY),
                    data.get(CURRENT_CAPACITY),
                    data.get(LOADING_CONTAINER_AMOUNT),
                    data.get(UNLOADING_CONTAINER_AMOUNT));
        } else {
            throw new IllegalObjectException(
                    "Ship with data " + data + "isn't exist.");
        }
    }
}
