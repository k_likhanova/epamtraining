package by.training.port.action;

import by.training.port.entity.Port;
import by.training.port.entity.Ship;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Semaphore;

/**
 * The type Work with containers in port.
 * Created on 14.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class WorkWithContainersInPort {
    /**
     * The constant specifies the logger.
     */
    private static final Logger LOG = LogManager.getLogger("logger");

    /**
     * The field specifies the port.
     */
    private final Port port = Port.getInstance();

    /**
     * The constant specifies the semaphore.
     */
    private static final Semaphore SEMAPHORE
            = new Semaphore(1, true);

    /**
     * Gets semaphore.
     *
     * @return the semaphore
     */
    public static Semaphore getSemaphore() {
        return SEMAPHORE;
    }

    /**
     * If there is enough capacity for unloading of the ship
     *      then method unload.
     * If available capacity in the port less than it is necessary
     *      then unloads so many containers how many holds.
     * If available capacity in the port more than it is necessary
     *      then unloads everything.
     *
     * @param ship the ship
     * @return whether it was unloaded
     */
    public boolean unloadShip(final Ship ship) {
        boolean isUnload = false;
        if (ship.getCurrentCapacity() != 0
                && port.getAvailableCapacity().get() != 0) {
            if (Long.compare(ship.getUnloadingContainersAmount(),
                    port.getAvailableCapacity().get()) > 0) {
                port.setCurrentCapacity(Port.getCapacity());
                LOG.log(Level.INFO, port.getAvailableCapacity().get()
                        + " containers was unloaded from the ship #"
                        + ship.getId());
                ship.setUnloadingContainersAmount(
                                    port.getAvailableCapacity().get());
            } else {
                port.setCurrentCapacity(port.getCurrentCapacity().get()
                        + ship.getUnloadingContainersAmount());
                LOG.log(Level.INFO, ship.getUnloadingContainersAmount()
                        + " containers was unloaded from the ship #"
                        + ship.getId());
                ship.setUnloadingContainersAmount(
                        ship.getUnloadingContainersAmount());
            }
            isUnload = true;
        }
        return isUnload;
    }

    /**
     * If there is enough capacity for loading of the ship
     *      then method load.
     * If containers in the port less than it is necessary
     *      then loads everything.
     * If containers in the port more than it is necessary
     *      then loads so many containers how many necessary.
     *
     * @param ship the ship
     * @return whether it was loaded
     */
    public boolean loadShip(final Ship ship) {
        boolean isLoad = false;
        if (ship.getAvailableCapacity() != 0
                && port.getCurrentCapacity().get() != 0) {
            if (Long.compare(ship.getLoadingContainersAmount(),
                    port.getCurrentCapacity().get()) > 0) {
                loadShip(ship, port.getCurrentCapacity().get());
            } else {
                loadShip(ship, ship.getLoadingContainersAmount());
            }
            isLoad = true;
        }
        return isLoad;
    }

    /**
     * If available capacity on the ship more than it is necessary
     * then loads so many containers how many necessary.
     * If available capacity on the ship less than it is necessary
     * then loads so many containers how many holds.
     *
     * @param ship the ship
     * @param loadingContainersAmount the amount of loading containers
     */
    private void loadShip(final Ship ship,
                          final long loadingContainersAmount) {
        if (Long.compare(ship.getAvailableCapacity(),
                loadingContainersAmount) >= 0) {
            port.setCurrentCapacity(port.getCurrentCapacity().get()
                                          - loadingContainersAmount);
            LOG.log(Level.INFO, loadingContainersAmount
                    + " containers was loaded to the ship #" + ship.getId());
            ship.setLoadingContainersAmount(loadingContainersAmount);
        } else {
            port.setCurrentCapacity(port.getCurrentCapacity().get()
                    - ship.getAvailableCapacity());
            LOG.log(Level.INFO, ship.getAvailableCapacity()
                    + " containers was loaded to the ship #" + ship.getId());
            ship.setLoadingContainersAmount(ship.getAvailableCapacity());
        }
    }
}
