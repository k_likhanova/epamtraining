package by.training.port.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The type File strings validator.
 * Created on 24.10.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public final class FileValidator {
    /**
     * Regular expression for ship data.
     */
    public static final String FILE_STRING_PATTERN =
            "(\\s*\\d+\\s*){5}";

    /**
     * Default constructor.
     */
    private FileValidator() {
    }

    /**
     * Validate line boolean.
     *
     * @param fileString the parameter
     * @return the boolean
     */
    public static boolean validateLine(final String fileString) {
        Pattern pattern = Pattern.compile(FILE_STRING_PATTERN);
        Matcher matcher = pattern.matcher(fileString);
        return matcher.matches();
    }
}
