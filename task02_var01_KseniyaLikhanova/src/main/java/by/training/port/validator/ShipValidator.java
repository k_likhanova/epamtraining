package by.training.port.validator;

import java.util.List;

/**
 * The type Ship validator.
 * Created on 13.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public final class ShipValidator {

    /**
     * The constant specifies number of element.
     */
    private static final int CAPACITY = 1;
    /**
     * The constant specifies number of element.
     */
    private static final int CURRENT_CAPACITY = 2;
    /**
     * The constant specifies number of element.
     */
    private static final int LOADING_CONTAINER_AMOUNT = 3;
    /**
     * The constant specifies number of element.
     */
    private static final int UNLOADING_CONTAINER_AMOUNT = 4;

    /**
     * Private constructor, because all method is static.
     */
    private ShipValidator() {
    }

    /**
     * Defines whether the data is ship.
     *
     * @param data the data
     * @return the boolean
     */
    public static boolean isCorrectData(final List<Long> data) {
        boolean isCorrectData = true;
        if (data.get(CAPACITY) == 0
            || Long.compare(
                            data.get(CURRENT_CAPACITY)
                                    + data.get(LOADING_CONTAINER_AMOUNT),
                            data.get(UNLOADING_CONTAINER_AMOUNT))
                    < 0
            || Long.compare(
                            data.get(CURRENT_CAPACITY)
                                + data.get(LOADING_CONTAINER_AMOUNT)
                                - data.get(UNLOADING_CONTAINER_AMOUNT),
                            data.get(CAPACITY))
                    > 0) {
            isCorrectData = false;
        }
        return isCorrectData;
    }
}
