package by.training.port.entity;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The type Port.
 * Created on 11.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public final class Port {

    /**
     * The constant specifies the amount of mooring.
     */
    private static final int MOORING_AMOUNT = 6;
    /**
     * The constant specifies the capacity of Port.
     */
    private static final long CAPACITY = 1500;
    /**
     * The constant specifies the semaphore.
     */
    private static final Semaphore SEMAPHORE
                    = new Semaphore(MOORING_AMOUNT, true);

    /**
     * Field instance specifies the single object of Port.
     */
    private static Port instance;
    /**
     * Field specifies whether the port is created.
     */
    private static AtomicBoolean isCreated = new AtomicBoolean(false);
    /**
     * The constant specifies the lock.
     */
    private static final ReentrantLock LOCK = new ReentrantLock();

    /**
     * Field specifies current capacity of Port.
     */
    private AtomicLong currentCapacity;
    /**
     * Field specifies current capacity of Port.
     */
    private AtomicLong availableCapacity;

    /**
     * Instantiates a new Port.
     */
    private Port() {
        currentCapacity = new AtomicLong(0);
        availableCapacity = new AtomicLong(CAPACITY);
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static Port getInstance() {
        if (!isCreated.get()) {
            try {
                LOCK.lock();
                if (instance == null) {
                    instance = new Port();
                    isCreated.set(true);
                }
            } finally {
                LOCK.unlock();
            }
        }
        return instance;
    }

    /**
     * Gets mooring amount.
     *
     * @return the mooring amount
     */
    public static int getMooringAmount() {
        return MOORING_AMOUNT;
    }

    /**
     * Gets capacity.
     *
     * @return the capacity
     */
    public static long getCapacity() {
        return CAPACITY;
    }

            /**
             * Gets semaphore.
             *
             * @return the semaphore
             */
            public static Semaphore getSemaphore() {
                return SEMAPHORE;
            }

    /**
     * Gets current capacity.
     *
     * @return the current capacity
     */
    public AtomicLong getCurrentCapacity() {
        return currentCapacity;
    }

    /**
     * Set current capacity and available capacity.
     *
     * @param newCurrentCapacity the new current capacity
     */
    public void setCurrentCapacity(final long newCurrentCapacity) {
        this.currentCapacity.set(newCurrentCapacity);
        this.availableCapacity.set(CAPACITY - newCurrentCapacity);
    }

    /**
     * Gets available capacity.
     *
     * @return the available capacity
     */
    public AtomicLong getAvailableCapacity() {
        return availableCapacity;
    }
}
