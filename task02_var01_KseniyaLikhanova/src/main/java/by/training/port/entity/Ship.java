package by.training.port.entity;

/**
 * The type Ship.
 * Created on 11.11.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class Ship {

    /**
     * Field specifies id of Ship.
     */
    private long id;
    /**
     * Field specifies capacity of Ship.
     */
    private long capacity;
    /**
     * Field specifies current capacity of Ship.
     */
    private long currentCapacity;
    /**
     * Field specifies available capacity of Ship.
     */
    private long availableCapacity;
    /**
     * Field specifies amount of containers for loading.
     */
    private long loadingContainersAmount;
    /**
     * Field specifies amount of containers for unloading.
     */
    private long unloadingContainersAmount;

    /**
     * Instantiates a new Ship.
     */
    public Ship() {
    }

    /**
     * Instantiates a new Ship.
     *
     * @param newId                        the new id
     * @param newCapacity                  the new capacity
     * @param newCurrentCapacity           the new current capacity
     * @param newLoadingContainersAmount   the new loading containers amount
     * @param newUnloadingContainersAmount the new unloading containers amount
     */
    public Ship(final long newId,
                final long newCapacity,
                final long newCurrentCapacity,
                final long newLoadingContainersAmount,
                final long newUnloadingContainersAmount) {
        this.id = newId;
        this.capacity = newCapacity;
        this.availableCapacity = newCapacity;
        this.currentCapacity = newCurrentCapacity;
        this.loadingContainersAmount = newLoadingContainersAmount;
        this.unloadingContainersAmount = newUnloadingContainersAmount;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param newId the new id
     */
    public void setId(final long newId) {
        this.id = newId;
    }

    /**
     * Gets capacity.
     *
     * @return the capacity
     */
    public long getCapacity() {
        return capacity;
    }

    /**
     * Gets current capacity.
     *
     * @return the current capacity
     */
    public long getCurrentCapacity() {
        return currentCapacity;
    }

    /**
     * Sets current capacity and available capacity.
     *
     * @param newCurrentCapacity the new current capacity
     */
    public void setCurrentCapacity(final long newCurrentCapacity) {
        this.currentCapacity = newCurrentCapacity;
        this.availableCapacity = capacity - newCurrentCapacity;
    }

    /**
     * Gets available capacity.
     *
     * @return the available capacity
     */
    public long getAvailableCapacity() {
        return availableCapacity;
    }

    /**
     * Gets amount of containers for loading.
     *
     * @return the amount of containers for loading
     */
    public long getLoadingContainersAmount() {
        return loadingContainersAmount;
    }

    /**
     * Sets amount of containers for loading and current capacity after load.
     *
     * @param loadedContainersAmount the amount of loaded containers
     */
    public void setLoadingContainersAmount(final long loadedContainersAmount) {
        this.loadingContainersAmount = loadingContainersAmount
                                        - loadedContainersAmount;
        setCurrentCapacity(currentCapacity + loadedContainersAmount);
    }

    /**
     * Gets amount of containers for unloading.
     *
     * @return the amount of containers for unloading
     */
    public long getUnloadingContainersAmount() {
        return unloadingContainersAmount;
    }

    /**
     * Sets amount of containers for unloading
     *      and current capacity after unload.
     *
     * @param unloadedContainersAmount the amount of unloaded containers
     */
    public void setUnloadingContainersAmount(
                                    final long unloadedContainersAmount) {
        this.unloadingContainersAmount = unloadingContainersAmount
                                          - unloadedContainersAmount;
        setCurrentCapacity(currentCapacity - unloadedContainersAmount);
    }
}
